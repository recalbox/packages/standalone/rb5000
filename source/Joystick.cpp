//
// Created by bkg2k on 12/10/22.
//

#include <SDL2/SDL.h>
#include "Joystick.h"
#include "recalbox-utils/Log.h"

Joystick::Joystick(const Configuration& configuration)
  : mConfiguration(configuration)
  , mJoysticks { nullptr, nullptr }
  , mPorts { 0xFF, 0xFF }
  , mHotkey(false)
  , mStart(false)
{
}

Joystick::~Joystick()
{
  for (int i = Configuration::sJoystickCount; --i >= 0;)
    if (mJoysticks[1] != nullptr) SDL_JoystickClose(mJoysticks[1]);
}

void Joystick::ProcessJoystickEvent(const SDL_Event& event)
{
  switch(event.type)
  {
    case SDL_JOYBUTTONDOWN:
    case SDL_JOYBUTTONUP:
    {
      int index = -1;
      for(int i = Configuration::sJoystickCount; --i >= 0; ) if (event.jbutton.which == mConfiguration.JoystickSDLIndex(i)) index = i;
      if (index < 0) return;
      if (index == 0)
      {
        // Hotkey
        if (event.jbutton.button == mConfiguration.JoystickHotkey()) mHotkey = event.jbutton.state != 0;
        // Start
        if (event.jbutton.button == mConfiguration.JoystickStart()) mStart = event.jbutton.state != 0;
      }
      // button 0
      for(int i = Configuration::sButtonsPerPlayer; --i >= 0; )
        if (event.jbutton.button == mConfiguration.JoystickButtons(index, i))
        {
          if (event.jbutton.state != 0) mPorts[index] &= ~(0x10 << i);
          else                          mPorts[index] |= 0x10 << i;
        }
        else if (event.jbutton.button == mConfiguration.JoystickUp(index))
        {
          if (event.jbutton.state != 0) mPorts[index] &= 0xFE;
          else                          mPorts[index] |= 0x01;
        }
        else if (event.jbutton.button == mConfiguration.JoystickRight(index))
        {
          if (event.jbutton.state != 0) mPorts[index] &= 0xFD;
          else                          mPorts[index] |= 0x02;
        }
        else if (event.jbutton.button == mConfiguration.JoystickDown(index))
        {
          if (event.jbutton.state != 0) mPorts[index] &= 0xFB;
          else                          mPorts[index] |= 0x04;
        }
        else if (event.jbutton.button == mConfiguration.JoystickLeft(index))
        {
          if (event.jbutton.state != 0) mPorts[index] &= 0xF7;
          else                          mPorts[index] |= 0x08;
        }
      break;
    }
    case SDL_JOYHATMOTION:
    {
      int index = -1;
      for(int i = Configuration::sJoystickCount; --i >= 0; ) if (event.jbutton.which == mConfiguration.JoystickSDLIndex(i)) index = i;
      if (index < 0) return;
      if ((event.jhat.value & 1) != 0) mPorts[index] &= 0xFE; else mPorts[index] |= 0x01;
      if ((event.jhat.value & 2) != 0) mPorts[index] &= 0xFD; else mPorts[index] |= 0x02;
      if ((event.jhat.value & 4) != 0) mPorts[index] &= 0xFB; else mPorts[index] |= 0x04;
      if ((event.jhat.value & 8) != 0) mPorts[index] &= 0xF7; else mPorts[index] |= 0x08;
      break;
    }
    case SDL_JOYAXISMOTION:
    {
      int index = -1;
      for(int i = Configuration::sJoystickCount; --i >= 0; ) if (event.jbutton.which == mConfiguration.JoystickSDLIndex(i)) index = i;
      if (index < 0) return;
      if (event.jaxis.axis == 0)
      {
        if (event.jaxis.value < -16383) mPorts[index] &= 0xF7; else mPorts[index] |= 0x08;
        if (event.jaxis.value >  16383) mPorts[index] &= 0xFD; else mPorts[index] |= 0x02;
      }
      if (event.jaxis.axis == 1)
      {
        if (event.jaxis.value < -16383) mPorts[index] &= 0xFE; else mPorts[index] |= 0x01;
        if (event.jaxis.value >  16383) mPorts[index] &= 0xFB; else mPorts[index] |= 0x04;
      }
      break;
    }
    default: break;
  }
}

void Joystick::Initialize()
{
  SDL_InitSubSystem(SDL_INIT_JOYSTICK);
  SDL_JoystickEventState(SDL_ENABLE);
  SDL_JoystickUpdate();
  if (SDL_NumJoysticks() == 0) LOG(LogWarning) << "[Joystick] No joystick";

  for(int j = SDL_NumJoysticks(); --j >= 0;)
  {
    for(int p = Configuration::sJoystickCount; --p >= 0; )
      if (j == mConfiguration.JoystickSDLIndex(p))
      {
        LOG(LogInfo) << "[Joystick] Joystick for player " << (p + 1) << " opened!";
        mJoysticks[p] = SDL_JoystickOpen(j);
      }
  }
}
