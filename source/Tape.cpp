//
// Created by bkg2k on 10/10/22.
//

#include "Tape.h"

bool Tape::ReadByte(byte& read)
{
  if (mContent.empty()) return false;
  if (mIndex >= (int)mContent.size()) return false;
  read = mContent[mIndex++];
  return true;
}

bool Tape::WriteByte(byte written)
{
  if (mConfiguration.TapeFileName().empty()) return false;
  if (!mConfiguration.TapeWritable()) return false;
  if (mIndex >= (int)mContent.size()) { mContent.Append(written); mIndex++; }
  else mContent[mIndex++] = (char)written;
  return true;
}

void Tape::Rewind(int amount)
{
  if ((mIndex -= amount) < 0) mIndex = 0;
}

void Tape::FastForward(int amount)
{
  if ((mIndex += amount) > (int)mContent.size()) mIndex = (int)mContent.size();
}
