//
// Created by bkg2k on 12/06/22.
//
#pragma once

#include "../Z80/Z80.h"
#include "KeyboardMatrix.h"
#include "Printer.h"
#include "Tape.h"
#include "recalbox-utils/Mutex.h"
#include "EF9345.h"
#include "Joystick.h"

class Engine
{
  public:
    explicit Engine(Configuration& configuration);

    //! Start engine
    void Start();

    //! Quit?
    [[nodiscard]] bool QuitRequested() const;

    //! Is emulated system ready?
    [[nodiscard]] bool IsEmulatedSystemReady() const { return mSystemReady; }

    //! Pause emulation
    void Pause() { mCPUPaused = true; }
    //! Unpause emulation
    void Unpause() { mCPUPaused = false; }
    //! Toggle pause emulation
    void TogglePause() { mCPUPaused = !mCPUPaused; }
    //! Paused?
    [[nodiscard]] bool IsPaused() const { return mCPUPaused; }

    /*!
     * @brief Process keyboard events
     * @param event Keyboard event
     */
    void ProcessKeyboard(const SDL_KeyboardEvent& event);

    /*!
     * @brief Process all joystick events
     * @param event Joystick event
     */
    void ProcessJoystick(const SDL_Event& event) { mJoystick.ProcessJoystickEvent(event); }

    /*!
     * @brief Process keystrockes to auto run tape program
     */
    void ProcessTapeAutoload();

    /*!
     * @brief Run the given amount of cycles
     * @param cycles cycle count
     */
    void RunCycles(int cycles);

    //! Soft reset the CPU
    void SoftReset();

    //! Hard reset the machine
    void HardReset();

    // Request output screen refresh
    bool RequestVideoRefresh() { return mVideo.RefreshWindow(); }

  private:
    //! CPU cycles per screen line
    static constexpr int sCyclesPerLine  = 400000 / 31;
    //! CPU cycles between IRQs
    static constexpr int sCyclesPerIRQ   = 80000; // 20ms
    //! CPU cycles between blinks
    static constexpr int sCyclesPerBlink = 2000000; // 500ms

    //! Raw memory space
    byte mMemory[0x10000];
    //! Raw IO port space
    byte mIOPort[0x100];
    //! Z80 State
    Z80 mCPU;

    //! Current screen line to draw
    int mNLine;
    //! Cycles elapsed since last line draw
    int mCycleLine;
    //! Cycles elapsed since last IRQ
    int mCycleIRQ;
    //! Cycles elapsed since last blink
    int mCycleBlink;
    //! Ouput sound level
    int mSoundLevel;
    //! Tape Autoload Index
    int mTapeAutoLoadIndex;
    //! Tape Autoload time reference
    int mTapeAutoLoadTimeReference;
    //! Keyboard readings (from emulated system)
    int mKeyboardReadings;
    //! Emulated system is ready
    bool mSystemReady;
    //! Global CPU pause
    bool mCPUPaused;

    //! Configuration reference
    Configuration& mConfiguration;

    //! Keyboard
    KeyboardMatrix mKeyboard;
    //! Joysticks
    Joystick mJoystick;
    //! Printer
    Printer mPrinter;
    //! Tape
    Tape mTape;
    //! Video
    Video mVideo;
    //! GPU
    EF9345 mGPU;

    //! Audio runner
    SDL_AudioSpec mAudioRunner;

    //! Thread-safe guardian
    Mutex mGuardian;

    //! Mirror
    static unsigned short sMirror[Video::PixelSize()];
    //! Mirror mapper
    bool mMapper[0x4000];

    /*!
     * @brief Run emulation & sound in SDL sound loop
     * @param instance this class instance
     * @param stream stream
     * @param bufferlength buffer sikze
     */
    static void EmulationLoopHandler(void* instance, unsigned char* stream, int bufferlength);

    /*!
     * @brief Run emulation & sound in SDL sound loop
     * @param stream stream
     * @param bufferlength buffer sikze
     */
    void EmulationLoop(unsigned char* stream, int bufferlength);

    /*!
     * @brief Process emulator keys
     * @param event Key event
     * @return True if the key has been processed
     */
    bool ProcessEmulationKeys(const SDL_KeyboardEvent& event);

    /*
     * Z80 callback
     */

    /*!
     * @brief Called when the CPU emulator reads a byte from memory
     * @param address Address
     * @return Byte read
     */
    [[nodiscard]] byte Z80Read(word address) { if (address >= 0x4000 || mMapper[address]) return mMemory[address]; return Z80ReadW(address); }

    /*!
     * @brief Called when the CPU emulator reads a byte from memory
     * @param address Address
     * @return Byte read
     */
    [[nodiscard]] byte Z80ReadW(word address);

    /*!
     * @brief Called when the CPU emulator write a byte to the given address
     * @param address Address
     * @param data Data
     */
    void Z80Write(word address, byte data) { if (address >= 0x4000) mMemory[address] = data; }

    /*!
     * @brief Called when the CPU emulator reads a byte from I/O ports
     * @param address Address
     * @return Byte read
     */
    [[nodiscard]] byte Z80ReadPort(word address);

    /*!
     * @brief Called when the CPU emulator write a byte to the given I/O port
     * @param address Address
     * @param data Data
     */
    void Z80WritePort(word address, byte data);

    /*!
     * @brief Z80 Emulator periodic hendler
     * @param R Z80 structure
     * @return IRQ status
     */
    word Z80Loop(Z80& R);

    /*!
     * @brief Allow to patch Z80 execution at runtime
     * @param R Z80 structure
     */
    void Z80Patch(Z80& R);

    /*
     * Z80 original callbacks
     */

    friend void WrZ80(void* userdata, word Addr, byte Value);
    friend byte RdZ80(void* userdata, word Addr);
    friend void OutZ80(void* userdata, word Port, byte Value);
    friend byte InZ80(void* userdata, word Port);
    friend word LoopZ80(register Z80 *R);
    friend void PatchZ80(register Z80 *R);
};
