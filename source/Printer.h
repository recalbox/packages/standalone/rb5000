//
// Created by bkg2k on 10/10/22.
//
#pragma once

#include "Configuration.h"
#include "../Z80/Z80.h"

class Printer
{
  public:
    Printer(const Configuration& configuration)
      : mConfiguration(configuration)
    {
    }

    void PrintByte(byte data);

  private:
    //! Configuration reference
    const Configuration& mConfiguration;
};
