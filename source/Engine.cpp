//
// Created by bkg2k on 12/06/22.
//

#include "Engine.h"
#include "recalbox-utils/Log.h"

#ifdef DEBUG
  static Engine* control = (Engine*)1;
  void _(void* userdata)
  {
    if (userdata != (void*)control)
    {
      abort();
    }
  }
#else
  #define _(userdata)
#endif

unsigned short Engine::sMirror[Video::PixelSize()];

Engine::Engine(Configuration& configuration)
  : mMemory()
  , mIOPort()
  , mCPU()
  , mNLine(0)
  , mCycleLine(0)
  , mCycleIRQ(0)
  , mCycleBlink(0)
  , mSoundLevel(0)
  , mTapeAutoLoadIndex(-1)
  , mTapeAutoLoadTimeReference(0)
  , mKeyboardReadings(0)
  , mSystemReady(false)
  , mCPUPaused(true)
  , mConfiguration(configuration)
  , mKeyboard(mIOPort)
  , mJoystick(configuration)
  , mPrinter(configuration)
  , mTape(configuration)
  , mVideo(configuration)
  , mGPU(mVideo)
  , mAudioRunner()
  , mMapper()
{
  #ifdef DEBUG
  control = this;
  #endif
  memset(&mMemory, 0, sizeof(mMemory));
  memset(&mIOPort, 0, sizeof(mIOPort));
  memset(&mCPU, 0, sizeof(mCPU));
  memset(&mMapper, 0, sizeof(mMapper));
  memcpy(sMirror, Video::Pixels(), Video::PixelSize());
}

void Engine::Start()
{
  // Joysticks
  mJoystick.Initialize();
  // Video!
  mVideo.Initialize();

  // load tape
  mTape.LoadTape();
  // auto load?
  if (mTape.IsTapeLoaded() && mConfiguration.TapeAutoRun())
  {
    mTapeAutoLoadIndex = 0;
    mTapeAutoLoadTimeReference = (int)SDL_GetTicks();
    LOG(LogInfo) << "[Engine] Tape auto-loading started!";
  }

  HardReset();

  // Run emulation/audio loop
  if (SDL_Init(SDL_INIT_AUDIO) < 0)
  {
    LOG(LogError) << "[Engine] Error initializing SDL Sound engine!";
    abort();
  }
  mAudioRunner.freq = 22050;
  mAudioRunner.format = AUDIO_U8;
  mAudioRunner.channels = 1;
  mAudioRunner.samples = 1024;
  mAudioRunner.callback = EmulationLoopHandler;
  mAudioRunner.userdata = this;
  if (SDL_OpenAudio(&mAudioRunner, nullptr) < 0)
  {
    LOG(LogError) << "Error starting SDL Sound engine!";
    abort();
  }
  // Run sound & emulation thread
  SDL_PauseAudio(0);
}

void Engine::EmulationLoopHandler(void* instance, unsigned char* stream, int bufferlength)
{
  ((Engine*)instance)->EmulationLoop(stream, bufferlength);
}

void Engine::EmulationLoop(unsigned char* stream, int bufferlength)
{
  // Cycles Z80 * (4000000 * coeff. acc.) / 22000
  int icycles = 1814 * mConfiguration.CPUSpeedKHz() / 10000;
  for(int i = 0; i < bufferlength; i++)
  {
    if (IsPaused()) { stream[i] = 128; continue;}
    {
      Mutex::AutoLock locker(mGuardian);
      RunCycles(icycles);
      if (IsEmulatedSystemReady()) ProcessTapeAutoload();
    }
    stream[i] = mSoundLevel + 128;
  }
}

void Engine::SoftReset()
{
  LOG(LogInfo) << "[Engine] Soft Reset!";
  mCPU.PC.W = 0x0033;
  mSoundLevel = 0;
  // Initalize joystick values
  mIOPort[0x07] = mIOPort[0x08] = 0xff;
  // Initialize keyboard values
  memset(&mIOPort[0x80], 0xFF, 8);
}

// Hardreset de l'ordinateur �mul� ///////////////////////////////////////////
void Engine::HardReset()
{
  LOG(LogInfo) << "[Engine] Hard Reset!";

  mGPU.Initialize();

  // Init memory space
  memset(mMemory, 0, sizeof(mMemory));
  //memcpy(mMemory, VG5000Firmware, sizeof(VG5000Firmware));
  memset(mIOPort, 0, std::size(mIOPort));

  // Soft reset
  SoftReset();

  // Reset CPU
  memset(&mCPU, 0, sizeof(mCPU));
  mCPU.User = this;
  ResetZ80(&mCPU);

  // Reset cycle counters
  mCycleLine = 0;
  mNLine = 0;
  mCycleIRQ = 0;
  mCycleBlink = 0;

  // Restart
  Unpause();
}

void Engine::RunCycles(int cycles)
{
  int previousCycles = mCPU.ICount;
  mCPU.IPeriod = cycles;
  RunZ80(&mCPU);
  // Ajust elapsed cycles
  cycles += previousCycles + mCPU.ICount;

  mGPU.ConsumeCycles(cycles);
  mCycleLine += cycles;
  mCycleIRQ += cycles;
  mCycleBlink += cycles;
}

byte Engine::Z80ReadPort(word address)
{
  address &= 0xff;
  // EF9345
  if(address == 0xcf) return mGPU.Read(mIOPort[0x8f]);
  // Keyboard
  if((address > 0x7f) && (address < 0x88))
  {
    // When the OS has read the whole keyboard once,
    // we consider the system is ready
    if (++mKeyboardReadings > 8)
    {
      if (!mSystemReady) LOG(LogInfo) << "[Engine] Emulated system ready.";
      mSystemReady = true;
    }
    return mIOPort[address];
  }
  // Joysticks
  if((address == 0) || (address == 1)) return mJoystick.JoystickPort(1 - address);
  if((address == 7) || (address == 8)) return mJoystick.JoystickPort(address - 7);
  return 0;
}

void Engine::Z80WritePort(word address, byte data)
{
  address &= 0xff;
  mIOPort[address] = data;
  // Printer
  if(address == 0x11) { mPrinter.PrintByte(data); return; }
  // EF9345
  if(address == 0xcf) { mGPU.Write(mIOPort[0x8f], data); return; }
  // Tape
  if(address == 0xaf) { mSoundLevel = data << 2; return; }
  //if(address == 0xef) { return; } //?????
}

word Engine::Z80Loop(Z80& R)
{
  _(R.User);
  // IRQ
  if(mCycleIRQ > sCyclesPerIRQ) { mCycleIRQ -= sCyclesPerIRQ; IntZ80(&R, INT_IRQ); }
  // Cursor blinking
  if(mCycleBlink > sCyclesPerBlink) { mCycleBlink -= sCyclesPerBlink; mGPU.Blinking(); }
  // Line drawing
  if(mCycleLine > sCyclesPerLine)    // affichage d'une ligne
  {
    mCycleLine -= sCyclesPerLine; mGPU.Displayline(mNLine++);
    // Screen ready for refresh?
    if(mNLine > 30)
    {
      mNLine = 0;
      mVideo.FrameReady();
    }
  }
  return INT_QUIT;
}

void Engine::Z80Patch(Z80& R)
{
  word pc = R.PC.W;
  _(R.User);
  // Check tape speed
  if (pc == 0x3af5)
  {
    R.AF.W = 0x0000; // Clear A & Flags
    R.PC.W = 0x3b47; // RET from routine
    return;
  }
  if (pc == 0x3aab)
  {
    mTape.WriteByte((byte)(R.AF.W >> 8));
    //mTape.WriteByte((byte)(R.AF.W >> 8));
    R.PC.W = 0x3acf; // RET from routine
    return;
  }
  if (pc == 0x3b4A)
  {
    byte result = 0;
    if (!mTape.ReadByte(result)) result = 0;
    //if (!mTape.ReadByte(result)) result = 0;
    R.AF.W = result << 8; // Read byte in A, clear flags
    R.PC.W = 0x3b8d; // RET from routine
    return;
  }
}

void WrZ80(void* userdata, word w, byte c) { _(userdata); ((Engine*)userdata)->Z80Write(w, c); }
byte RdZ80(void* userdata, word w) { _(userdata);  return ((Engine*)userdata)->Z80Read(w); }
void OutZ80(void* userdata, word w, byte c) { _(userdata);  ((Engine*)userdata)->Z80WritePort(w, c); }
byte InZ80(void* userdata, word w) { _(userdata);  return ((Engine*)userdata)->Z80ReadPort(w); }
word LoopZ80(register Z80 *R) { _(R->User); return ((Engine*)R->User)->Z80Loop(*R); }
void PatchZ80(register Z80 *R) { _(R->User); if ((R->PC.W & 0xFE00) == 0x3a00) ((Engine*)R->User)->Z80Patch(*R); }

void Engine::ProcessKeyboard(const SDL_KeyboardEvent& event)
{
  if (!ProcessEmulationKeys(event))
    mKeyboard.ProcessSDL2Event(event);
}

bool Engine::ProcessEmulationKeys(const SDL_KeyboardEvent& event)
{
  if (event.state != 0)
  {
    Mutex::AutoLock locker(mGuardian);
    switch(event.keysym.sym)
    {
      case SDLK_F2:
      {
        mConfiguration.SlowDown();
        LOG(LogInfo) << "[Engine] Slow down to " << (mConfiguration.CPUSpeedKHz() / 10) << '%';
        mVideo.Popup(String("Slow down to ").Append(mConfiguration.CPUSpeedKHz() / 10).Append('%'));
        return true;
      }
      case SDLK_F3:
      {
        mConfiguration.SpeedUp();
        LOG(LogInfo) << "[Engine] Speed up to " << ((float)mConfiguration.CPUSpeedKHz() / 10.f) << '%';
        mVideo.Popup(String("Speed up to ").Append(mConfiguration.CPUSpeedKHz() / 10).Append('%'));
        return true;
      }
      case SDLK_F4:
      {
        mConfiguration.DefaultSpeed();
        LOG(LogInfo) << "[Engine] Speed set to default";
        mVideo.Popup(String("Speed set to default"));
        return true;
      }
      case SDLK_F5: mTape.Rewind(INT32_MAX); return true;
      case SDLK_F6: mTape.Rewind((event.keysym.mod & KMOD_SHIFT) != 0 ? 1 : 512); return true;
      case SDLK_F7: mTape.FastForward((event.keysym.mod & KMOD_SHIFT) != 0? 1 : 512); return true;
      case SDLK_F8: mTape.FastForward(INT32_MAX); return true;
      case SDLK_ESCAPE:
      {
        SDL_Event quit; quit.type = SDL_QUIT;
        SDL_PushEvent(&quit);
        LOG(LogInfo) << "[Engine] Quit requested (keyboard)";
        return true;
      }
      case SDLK_F11:
      {
        LOG(LogInfo) << "[Engine] Hard reset requested";
        mVideo.Popup("Hard Reset!");
        HardReset();
        return true;
      }
      case SDLK_F12:
      {
        LOG(LogInfo) << "[Engine] Soft reset requested";
        mVideo.Popup("Soft Reset!");
        SoftReset();
        return true;
      }
      case SDLK_PAUSE:
      {
        TogglePause();
        LOG(LogInfo) << "[Engine] " << (IsPaused() ? "Pause" : "Resume");
        mVideo.Popup((IsPaused() ? "Pause" : "Resume"));
        return true;
      }
      default: break;
    }
  }
  return false;
}

void Engine::ProcessTapeAutoload()
{
  static constexpr int sRawKeyTimer = 75;

  static SDL_KeyCode sKeyStrokes[] =
  {
    SDLK_c,
    SDLK_l,
    SDLK_o,
    SDLK_a,
    SDLK_d,
    SDLK_RETURN,
    SDLK_UNKNOWN,
  };

  // Nope
  if (mTapeAutoLoadIndex < 0 || !mSystemReady) return;

  if (((int)SDL_GetTicks() - mTapeAutoLoadTimeReference) > sRawKeyTimer)
  {
    SDL_Event event;
    memset(&event, 0, sizeof(event));
    event.key.keysym.sym = sKeyStrokes[mTapeAutoLoadIndex >> 1];
    event.key.state = (mTapeAutoLoadIndex & 1) ^ 1;
    event.type = (mTapeAutoLoadIndex & 1) != 0 ? SDL_KEYUP : SDL_KEYDOWN;
    SDL_PushEvent(&event);

    mTapeAutoLoadTimeReference = (int)SDL_GetTicks();
    mTapeAutoLoadIndex++;
    // Stop
    if (sKeyStrokes[mTapeAutoLoadIndex >> 1] == SDLK_UNKNOWN) mTapeAutoLoadIndex = -1;
  }
}

bool Engine::QuitRequested() const
{
  if (mJoystick.QuitRequested())
    LOG(LogInfo) << "[Engine] Quit requested (joystick)";
  return mJoystick.QuitRequested();
}

byte Engine::Z80ReadW(word address)
{
  byte memoryMapper = 0;
  mMapper[address] = true;
  unsigned short* s = &sMirror[address * 8];
  for(int j = 8 ; --j>= 0;)
  {
    unsigned short mirrorData = s[j];
    memoryMapper = (memoryMapper << 1) | (((mirrorData >> 5) ^ mirrorData) & 1);
  }
  return mMemory[address] = memoryMapper;
}
