//
// Created by bkg2k on 09/10/22.
//

#include <SDL2/SDL_ttf.h>
#include "Video.h"
#include "recalbox-utils/gl/Mapper.h"
#include "recalbox-utils/Log.h"

Video::Video(const Configuration& configuration)
  : mRenderer()
  , mConfiguration(configuration)
  , mFont(nullptr)
  , mPixelsTexture(0)
  , mPopupTexture(0)
  , mPopupReference(0)
  , mPopupWidth(0)
  , mPopupHeight(0)
  , mWindowWidth(-1)
  , mWindowHeight(-1)
  , mFrameCounter(0)
  , mLastRefreshedFrame(0)
  , mWindowRatio(1.0)
  , mPalette()
{
  if (SDL_Init(SDL_INIT_VIDEO) < 0)
  {
    LOG(LogError) << "[Video] Error initializing SDL Video engine!";
    abort();
  }
  if (TTF_Init() < 0)
  {
    LOG(LogError) << "[Video] Error initializing TTF engine!";
    abort();
  }

  MakePalette();
}

Video::~Video()
{
  TTF_CloseFont(mFont);
  Renderer::DestroyGLTexture(mPixelsTexture);
}

void Video::Initialize()
{
  int width = mConfiguration.WindowWidth();
  int height= mConfiguration.WindowHeight();
  LOG(LogInfo) << "[Video] Got " << width << 'x' << height << " from configuration and is " << ((width | height) <= 0 ? "invalid!" : "valid");
  if ((width | height) <= 0)
  {
    GetBestResolution(width, height);
    LOG(LogInfo) << "[Video] Tried to figure out the best resolution and got " << width << 'x' << height << " from configuration and it is " << (width * height <= 0 ? " still invalid!" : "valid");
  }
  InitializeWindow(width, height, !mConfiguration.Fullscreen());
}

Video::Pixel Video::MakeColor(int r, int g, int b)
{
  // Color intensities w/ gamma correction
  static const byte gamma[16] = { 0, 60, 90, 110, 130, 148, 165, 180, 193, 205, 215, 225, 230, 235, 240, 255 };
  return { .V = (unsigned short)(((gamma[r] >> 3) << 11) | ((gamma[g] >> 2) << 5) | ((gamma[b] >> 3))) };
}

void Video::MakePalette()
{
  int r[sColorCount] = { 0, 15, 0, 15, 0, 15, 0, 15, 0, 15, 0, 15, 0, 15, 0, 15 };
  int v[sColorCount] = { 0, 0, 15, 15, 0, 0, 15, 15, 0, 0, 15, 15, 0, 0, 15, 15 };
  int b[sColorCount] = { 0, 0, 0, 0, 15, 15, 15, 15, 0, 0, 0, 0, 15, 15, 15, 15 };
  for(int i = 0; i < sColorCount; i++)
    mPalette[i] = MakeColor(r[i], v[i], b[i]);
}

void Video::InitializeWindow(int width, int height, bool windowed)
{
  // Open window/screen
  mWindowWidth = width;
  mWindowHeight = height;
  mRenderer.Initialize(mWindowWidth, mWindowHeight, windowed);
  mWindowRatio = (double)mWindowWidth / (double)mWindowHeight;

  // Create textures
  mPixelsTexture = Renderer::CreateGLTexture();
  mPopupTexture = Renderer::CreateGLTexture();

  // Create font
  int fontHeight = mWindowHeight / 30;
  if (fontHeight < 20) fontHeight = 20;
  mFont = TTF_OpenFont("/usr/share/fonts/truetype/ubuntu_condensed.ttf", fontHeight);
  if (mFont == nullptr)
    mFont = TTF_OpenFont("/usr/share/fonts/truetype/ubuntu/Ubuntu-C.ttf", fontHeight);

  // First display
  Renderer::SetMatrix(Transform4x4f::Identity());
  Renderer::FilterTexture(mPixelsTexture, mConfiguration.Smooth());
  FrameReady();
  RefreshWindow();
}

bool Video::RefreshWindow()
{
  if (mLastRefreshedFrame == mFrameCounter) return false;
  mLastRefreshedFrame = mFrameCounter;

  // Source rectangle - always static
  SDL_Rect source;
  source.x = source.y = 0;
  source.w = sWidth40; source.h = sHeight;

  // Destination rectangle - depends on window resolution
  SDL_Rect destination;
  if (mConfiguration.IntegerScale())
  {
    int x = mWindowWidth / sWidth40;
    int y = mWindowHeight / sHeight;
    int c = x < y ? x : y;
    destination.w = sWidth40 * c;
    destination.h = sHeight * c;
    destination.x = (mWindowWidth - destination.w) / 2;
    destination.y = (mWindowHeight - destination.h) / 2;
  }
  else
  {
    if (mWindowRatio < sRealRatio)
    {
      double height = mWindowWidth / sRealRatio;
      destination.x = 0;
      destination.y = (mWindowHeight - (int) height) / 2;
      destination.w = mWindowWidth;
      destination.h = (int) height;
    }
    else
    {
      double width = mWindowHeight * sRealRatio;
      destination.x = (mWindowWidth - (int) width) / 2;
      destination.y = 0;
      destination.w = (int) width;
      destination.h = mWindowHeight;
    }
  }

  Vertex::Rectangle vertexes;
  // Upload only the visible part into the texture
  Renderer::UploadRGB565(mPixelsTexture, sWidth40, sHeight, sPixels);
  // Prepare vertexes
  Rectangle textureSource((float)source.x, (float)source.y, (float)source.w, (float)source.h);
  if (mConfiguration.Smooth()) textureSource.Contract(.5f, .5f); // avoid GL sampling out of visible screen area
  Rectangle textureTarget((float)destination.x, (float)destination.y, (float)destination.w, (float)destination.h);
  // Blit
  Mapper::MapTexture(textureTarget, textureSource.FlipOnScreen(sHeight).FlipY(), (float)sWidth40, (float)sHeight, vertexes);
  Renderer::DrawTexturedTriangles(mPixelsTexture, vertexes, 0xFFFFFFFF, Vertex::sVertexPerRectangle, false);

  // Popups
  DisplayPopup(textureTarget);

  // Wait "VBL" and swap buffers
  mRenderer.SwapBuffers();
  return true;
}

void Video::DrawChar40(const unsigned char* charBuffer, int x, int y)
{
  unsigned short* p0 = &sPixels[(y * 10) * sWidth40];   //pointeur debut de la ligne courante
  int nsegment = 8 * x;                       //numero du segment courant
  int x0 = nsegment;                //position debut segment dans la ligne
  for(int i = 0; i < 10; i++)
  {
    unsigned short* p = p0 + x0;                           //pointeur segment courant
    nsegment = 8 * x;                      //numero du segment courant
    for(int j = 0; j < 8; j++)
    {
      unsigned short* p1 = p0 + ++nsegment;       //pointeur debut du segment suivant
      if (charBuffer[8 * i + j] != 0)
        while(p < p1) *p++ = mPalette[charBuffer[8 * i + j]].V; //memcpy(p++, mPalette + charBuffer[8 * i + j], 4);
    }
    p0 += sWidth40;
  }
}

void Video::Popup(const String& message)
{
  // Create new
  SDL_Surface* fpsText = TTF_RenderText_Blended(mFont, message.c_str(), SDL_Color { .r=0xFF, .g=0xFF, .b=0xFF, .a=0xFF }); //, SDL_Color { .r=0x00, .g=0x00, .b=0x00, .a=0xFF });
  SDL_Surface* fpsTextRGB = SDL_ConvertSurfaceFormat(fpsText, SDL_PIXELFORMAT_BGRA32, 0);
  mPopupWidth = fpsText->w;
  mPopupHeight = fpsText->h;
  Renderer::UploadRGBA(mPopupTexture, mPopupWidth, mPopupHeight, fpsTextRGB->pixels);
  SDL_FreeSurface(fpsText);
  SDL_FreeSurface(fpsTextRGB);

  // Take time reference
  mPopupReference = SDL_GetTicks();
}

void Video::DisplayPopup(const Rectangle& targetArea)
{
  if (mPopupReference != 0)
  {
    // Process
    int Alpha = (int)SDL_GetTicks() - (int)mPopupReference;
    if (Alpha < 1000) Alpha = 255;
    else
    {
      // Fade
      Alpha = (int)(((2000.f - ((float)Alpha - 1000.f)) / 2000.f) * 255.f);
      // End?
      if (Alpha <= 0) { mPopupReference = 0; return; }
    }
    // Display
    Vertex::Rectangle vertexes;
    Rectangle fpsRect(0, 0, (float) mPopupWidth, (float) mPopupHeight);
    Rectangle targetText(fpsRect);
    targetText.FlipY().Move(targetArea.Right() - (float)mPopupWidth - targetArea.Height() / 40.f,
                            targetArea.Bottom() - (float)mPopupHeight - targetArea.Height() / 40.f);
    Mapper::MapTexture(targetText, fpsRect, (float)mPopupWidth, (float)mPopupHeight, vertexes);
    Rectangle targetRect = Rectangle(targetText).FlipY().Expand(targetArea.Height() / 80.f, targetArea.Height() / 80.f);
    Renderer::DrawRectangle(targetRect, Colors::MakeARGB(0, 0, 0, Alpha >> 1));
    Renderer::DrawTexturedTriangles(mPopupTexture, vertexes, Colors::MakeARGB(Alpha, 0xFF, 0xFF, 0xFF), Vertex::sVertexPerRectangle, false);
  }
}

bool Video::GetBestResolution(int& width, int& height)
{
  width = height = 0;
  { LOG(LogDebug) << "[Video] Getting resolution from sdl"; }

  struct Rez { int width, height; };
  std::vector<Rez> Resolutions;

  // Record all
  SDL_DisplayMode mode = { SDL_PIXELFORMAT_UNKNOWN, 0, 0, 0, nullptr };
  for (int m = SDL_GetNumDisplayModes(0); --m >= 0;)
    if (SDL_GetDisplayMode(0, m, &mode) == 0)
      Resolutions.push_back( { mode.w, mode.h } );

  // Seek for best matching mode
  bool found = false;
  for(const Rez& r : Resolutions)
    if (r.width >= sWidth40 && r.height >= sHeight)
      if (!found || (r.height <= height && r.width < width))
      {
        width = r.width;
        height = r.height;
        found = true;
      }
  if (found) return true;

  // Not found, take the higher resolution
  int minDelta = INT32_MAX;
  for(const Rez& r : Resolutions)
  {
    const int widthDelta = (r.width - width) * (r.width - width);
    if (!found || (r.height >= height && widthDelta < minDelta))
    {
      minDelta = widthDelta;
      width = r.width;
      height = r.height;
      found = true;
    }
  }

  return false;
}



