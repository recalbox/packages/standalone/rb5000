//
// Created by bkg2k on 07/10/22.
//

#include <cstring>
#include "KeyboardMatrix.h"

void KeyboardMatrix::Reset()
{
  memset(mIOPorts, 0xFF, sMaxKeys >> 7);
}

void KeyboardMatrix::ProcessSDL2Event(const SDL_KeyboardEvent& event) const
{
  int keyRowColumn = -1;
  switch(event.keysym.sym)
  {
    // Digits
    case SDLK_1: keyRowColumn = 0x15; break;
    case SDLK_2: keyRowColumn = 0x1E; break;
    case SDLK_3: keyRowColumn = 0x1A; break;
    case SDLK_4: keyRowColumn = 0x1B; break;
    case SDLK_5: keyRowColumn = 0x1C; break;
    case SDLK_6: keyRowColumn = 0x1D; break;
    case SDLK_7: keyRowColumn = 0x2D; break;
    case SDLK_8: keyRowColumn = 0x2E; break;
    case SDLK_9: keyRowColumn = 0x2F; break;
    case SDLK_0: keyRowColumn = 0x29; break;
    // Shifts
    case SDLK_LSHIFT: keyRowColumn = 0x02; break;
    case SDLK_RSHIFT: keyRowColumn = 0x0C; break;
    case SDLK_RALT  : keyRowColumn = 0x0B; break;
    // Large keys
    case SDLK_LCTRL:
    case SDLK_RCTRL: keyRowColumn = 0x06; break;
    case SDLK_KP_ENTER:
    case SDLK_RETURN:
    case SDLK_RETURN2: keyRowColumn = 0x0D; break;
    case SDLK_PAGEUP: keyRowColumn = 0x01; break;
    // Cursor
    case SDLK_UP: keyRowColumn = 0x0E; break;
    case SDLK_DOWN: keyRowColumn = 0x05; break;
    case SDLK_LEFT: keyRowColumn = 0x03; break;
    case SDLK_RIGHT: keyRowColumn = 0x04; break;
    // Space
    case SDLK_SPACE: keyRowColumn = 0x0A; break;
    // Letters
    case SDLK_q: keyRowColumn = 0x09; break;
    case SDLK_w: keyRowColumn = 0x10; break;
    case SDLK_e: keyRowColumn = 0x19; break;
    case SDLK_r: keyRowColumn = 0x32; break;
    case SDLK_t: keyRowColumn = 0x33; break;
    case SDLK_y: keyRowColumn = 0x34; break;
    case SDLK_u: keyRowColumn = 0x24; break;
    case SDLK_i: keyRowColumn = 0x25; break;
    case SDLK_o: keyRowColumn = 0x26; break;
    case SDLK_p: keyRowColumn = 0x27; break;
    case SDLK_a: keyRowColumn = 0x0F; break;
    case SDLK_s: keyRowColumn = 0x18; break;
    case SDLK_d: keyRowColumn = 0x37; break;
    case SDLK_f: keyRowColumn = 0x22; break;
    case SDLK_g: keyRowColumn = 0x23; break;
    case SDLK_h: keyRowColumn = 0x3C; break;
    case SDLK_j: keyRowColumn = 0x3B; break;
    case SDLK_k: keyRowColumn = 0x3D; break;
    case SDLK_l: keyRowColumn = 0x3E; break;
    case SDLK_m: keyRowColumn = 0x3F; break;
    case SDLK_z: keyRowColumn = 0x17; break;
    case SDLK_x: keyRowColumn = 0x11; break;
    case SDLK_c: keyRowColumn = 0x12; break;
    case SDLK_v: keyRowColumn = 0x13; break;
    case SDLK_b: keyRowColumn = 0x14; break;
    case SDLK_n: keyRowColumn = 0x3A; break;
    // Special key under digits
    case SDLK_COMMA: keyRowColumn = 0x2C; break;
    case SDLK_SEMICOLON: keyRowColumn = 0x1F; break;
    case SDLK_COLON: keyRowColumn = 0x16; break;
    case SDLK_RIGHTPAREN: keyRowColumn = 0x2A; break;
    case SDLK_ASTERISK: keyRowColumn = 0x2B; break;
    case SDLK_LESS: keyRowColumn = 0x35; break;
    case SDLK_HOME: keyRowColumn = 0x36; break;
    // Right characters
    case SDLK_KP_PLUS: keyRowColumn = 0x31; break;
    case SDLK_KP_MINUS: keyRowColumn = 0x30; break;
    case SDLK_KP_MULTIPLY: keyRowColumn = 0x21; break;
    case SDLK_KP_DIVIDE: keyRowColumn = 0x20; break;
    case SDLK_EQUALS: keyRowColumn = 0x38; break;
    // Right non characters
    case SDLK_INSERT: keyRowColumn = 0x07; break;
    case SDLK_DELETE: keyRowColumn = 0x00; break;
    case SDLK_BACKSPACE: keyRowColumn = 0x39; break;
    case SDLK_END: keyRowColumn = 0x08; break;
    default: break;
  }

  if (keyRowColumn >= 0)
  {
    if (event.state != 0) mIOPorts[keyRowColumn >> 3] &= ~(1 << (keyRowColumn & 7));
    else mIOPorts[keyRowColumn >> 3] |= (1 << (keyRowColumn & 7));
  }
}
