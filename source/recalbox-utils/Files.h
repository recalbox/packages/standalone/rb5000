#pragma once

#include "String.h"

class Files
{
  private:
    static constexpr long sBufferSizeBits = 20;
    static constexpr long sBufferSize = 1 << sBufferSizeBits;
    static constexpr long sBufferSizeMask = (sBufferSize - 1);

  public:
    /*!
     * @brief Load the whole content of a file into a string
     * @param path File to load
     * @return File content
     */
    static String LoadFile(const String& path);

    /*!
     * @brief Save the given string into a file
     * @param path File path
     * @param content String ot save
     * @return True if the content has been saved
     */
    static bool SaveFile(const String& path, const std::string& content);

    /*!
     * @brief Append the given string at the end of the given file or create it if it does not exist
     * @param path File path
     * @param data Data buffer to append
     * @param size Data size to append
     * @return True if the content has been saved
     */
    static bool AppendToFile(const String& path, const void* data, int size);
};
