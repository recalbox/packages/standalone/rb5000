#include "Log.h"
#include "DateTime.h"
#include <sys/stat.h>

LogLevel Log::reportingLevel = LogLevel::LogDebug;
FILE* Log::sFile = nullptr;

static const char* StringLevel[] =
{
  "ERROR",
  "WARN!",
  "INFO ",
	"DEBUG",
};

void Log::open(const char* filename)
{
  // Build log path
  String logpath(filename != nullptr ? filename : "/recalbox/share/system/logs/rb5000.txt");

  // Open new log
  sFile = fopen(logpath.data(), "w");
}

Log& Log::get(LogLevel level)
{
	mMessage.Append('[')
	        .Append(DateTime().ToPreciseTimeStamp())
	        .Append(LEGACY_STRING("] {RB5000} ("))
	        .Append(StringLevel[(int)level])
	        .Append(") : ");
	messageLevel = level;

	return *this;
}

void Log::flush()
{
	fflush(sFile);
}

void Log::close()
{
  {
    // *DO NOT REMOVE* the enclosing block as it allow the destructor to be called immediately
    // before calling doClose()
    // Generate an immediate log.
    Log().get(LogLevel::LogInfo) << "Closing logger...";
  }
  doClose();
}

void Log::doClose()
{
  if (sFile != nullptr)
    fclose(sFile);
  sFile = nullptr;
}

Log::~Log()
{
	bool loggerClosed = (sFile == nullptr);
	// Reopen temporarily
	if (loggerClosed)
  {
	  open();
	  mMessage += " [closed!]";
  }

  mMessage += '\n';
	fputs(mMessage.c_str(), sFile);
	if (!loggerClosed) flush();
	else doClose();

  // if it's an error, also print to console
  // print all messages if using --debug
  if(messageLevel == LogLevel::LogError || reportingLevel >= LogLevel::LogDebug)
    fputs(mMessage.c_str(), stderr);
}
