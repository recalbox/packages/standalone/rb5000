//
// Created by bkg2k on 31/07/2019.
//

#include "Files.h"
#include "fcntl.h"
#include "unistd.h"

String Files::LoadFile(const String& path)
{
  String result;

  int fd = open(path.data(), O_RDONLY);
  if (fd >= 0)
  {
    long l = lseek(fd, 0, SEEK_END);
    if (l > 0)
    {
      result.resize(l, 0);
      lseek(fd, 0, SEEK_SET);
      l = read(fd, (void*)result.data(), l);
      if (l < 0) l = 0; // Return empty string on error
      result.resize(l);
    }
    else if (l < 0)
    {
      char buffer[4096];
      for(int r = 0; (r = (int)read(fd, buffer, sizeof(buffer))) > 0; )
        result.Append(buffer, r);
    }
    close(fd);
  }

  return result;
}

bool Files::SaveFile(const String& path, const std::string& content)
{
  FILE* f = fopen(path.data(), "wb");
  if (f != nullptr)
  {
    bool ok = (fwrite(content.c_str(), content.size(), 1, f) == 1);
    fclose(f);
    return ok;
  }
  return false;
}

bool Files::AppendToFile(const String& path, const void* data, int size)
{
  FILE* f = fopen(path.data(), "ab");
  if (f != nullptr)
  {
    bool ok = (fwrite(data, size, 1, f) == 1);
    fclose(f);
    return ok;
  }
  return false;
}

