#pragma once

#include "GLInclude.h"
#include <stack>
#include <SDL2/SDL.h>
#include "Vector2f.h"
#include "Transform4x4f.h"
#include "Rectangle.h"
#include "Vertex.h"
#include "Colors.h"

// Forward declatation
class Component;
class Font;

class Renderer
{
  private:
    //! SDL Surface
    SDL_Window*   mSdlWindow;
    //! SDL GL context
    SDL_GLContext mSdlGLContext;

    //! Display width as integer
    int   mDisplayWidth;
    //! Display height as integer
    int   mDisplayHeight;
    //! Display width as float
    float mDisplayWidthFloat;
    //! Display height as float
    float mDisplayHeightFloat;

    //! Initial cursor state
    bool mInitialCursorState;
    //! Windowed mode
    bool mWindowed;
    //! True if both surface and context have been initialized
    bool mViewPortInitialized;

    //! Last X translation
    static float mLastXTranslation;
    //! Last Y translation
    static float mLastYTranslation;

    /*!
     * @brief Create SDL display surface
     * @return True if the surface has been created successfuly
     */
    bool CreateSdlSurface();

    /*!
     * @brief Destroy SDL displaysurface
     */
    void DestroySdlSurface();

  public:
    //! Error status
    enum class Error
    {
        NoError,        //!< Everything is fine
        OutOfMemory,    //!< Out of memory (CPU)
        OutOfGPUMemory, //!< Out of memory (GPU)
        NoResource,     //!< File not found or unknown internal resource
    };

    /*!
     * @brief Constructor
     */
    Renderer();

    /*!
     * @brief Destructor
     */
    ~Renderer();

    /*!
     * @brief Initialize GL viewport
     * @param w Required Width (0 = display width)
     * @param h Required Height (0 = display height)
     * @param windowed True = window, false = fullscreen
     * @return true if everything is working fine, false otherwise
     */
    bool Initialize(int& w, int& h, bool windowed);

    /*!
     * Finalize GL viewport
     */
    void Finalize();

    /*!
     * @brief Applmy the given matrix to openGL context
     * @param transform Matrix
     */
    static void SetMatrix(const Transform4x4f& transform);

    /*!
     * @brief Swap working and dipslayed buffers in double buffering context
     */
    void SwapBuffers();

    /*
     * Drawing
     */

    /*!
     * @brief Draw rectangle
     * @param x Left coordinate
     * @param y Top coordinate
     * @param w Width
     * @param h Height
     * @param color Color
     * @param blend_sfactor Source blending
     * @param blend_dfactor Destination blending
     */
    static void DrawRectangle(const Rectangle& area, Colors::ColorARGB color, GLenum blend_sfactor = GL_SRC_ALPHA, GLenum blend_dfactor = GL_ONE_MINUS_SRC_ALPHA);

    /*!
     * @brief Draw rectangle
     * @param x Left coordinate
     * @param y Top coordinate
     * @param w Width
     * @param h Height
     * @param color Color
     * @param blend_sfactor Source blending
     * @param blend_dfactor Destination blending
     */
    static void DrawRectangle(int x, int y, int w, int h, Colors::ColorARGB color, GLenum blend_sfactor = GL_SRC_ALPHA, GLenum blend_dfactor = GL_ONE_MINUS_SRC_ALPHA);

    /*!
     * @brief Draw rectangle
     * @param x Left coordinate
     * @param y Top coordinate
     * @param w Width
     * @param h Height
     * @param color Color
     * @param blend_sfactor Source blending
     * @param blend_dfactor Destination blending
     */
    static void DrawRectangle(float x, float y, float w, float h, Colors::ColorARGB color, GLenum blend_sfactor = GL_SRC_ALPHA, GLenum blend_dfactor = GL_ONE_MINUS_SRC_ALPHA);

    /*!
     * @brief Draw polylines
     * @param vertices Vertice list
     * @param colors Color array
     * @param count Vertex count
     */
    static void DrawLines(const Vector2f vertices[], const Colors::ColorARGB colors[], int count);

    /*!
     * @brief Draw textured triangles
     * @param id GL texture id
     * @param vertices Vertice list
     * @param colors Color list
     * @param count Vertice count
     * @param tiled draw tiled texture
     */
    static void DrawTexturedTriangles(GLuint id, const Vertex vertices[], const GLubyte colors[], int count, bool tiled);

    /*!
     * @brief Draw textured triangles using a single color
     * @param id GL texture id
     * @param vertices Vertice list
     * @param color Color
     * @param count Vertice count
     * @param tiled draw tiled texture
     */
    static void DrawTexturedTriangles(GLuint id, const Vertex vertices[], Colors::ColorARGB color, int count, bool tiled);

    /*!
     * @brief Upload RGBA texture data to GPU memory
     * @param id GL Texture id
     * @param width Data width
     * @param height Data height
     * @param data RGB565 bytes (2 byte = 1 pixel)
     * @return NoError or OutOfGPUMemory
     */
    static Error UploadRGBA(GLuint id, int width, int height, const void* data);
    /*!
     * @brief Upload RGB565 texture data to GPU memory
     * @param id GL Texture id
     * @param width Data width
     * @param height Data height
     * @param data RGB565 bytes (2 byte = 1 pixel)
     * @return NoError or OutOfGPUMemory
     */
    static Error UploadRGB565(GLuint id, int width, int height, const void* data);

    /*!
     * @brief Upload RGB565 texture data to GPU memory
     * @param id GL Texture id
     * @param width Data width
     * @param height Data height
     * @param data RGB565 bytes (2 byte = 1 pixel)
     * @return NoError or OutOfGPUMemory
     */
    static Error UploadRGB565Part(GLuint id, int x, int y, int width, int height, const void* data);

    /*!
     * @brief Create a new texture and return its GL Identifier
     * @return GL Texture identifier
     */
    static GLuint CreateGLTexture();

    /*!
     * @brief Chcck one or more GL errors
     * @return True if there is a least one error, false otherwise
     */
    static bool CheckErrors();

    /*!
     * Cleanup GL errors
     */
    static void EmptyErrors();

    /*!
     * @brief Destroy the texture associated to the given id
     * @param id GL Texture identifier
     */
    static void DestroyGLTexture(GLuint id);

    /*!
     * @brief Set linear filtering for the given texture
     * @param id Texture id
     * @param smooth True to activate biliear filtering, false for nearest pixel
     */
    static void FilterTexture(GLuint id, bool smooth);

    /*!
     * @brief Build a GL color array
     * @param array Array pointer
     * @param color Color
     * @param count Count x Color => array
     */
    static void BuildGLColorArray(GLubyte* array, Colors::ColorARGB color, int Count);

    static void ColorToByteArray(GLubyte* array, Colors::ColorARGB color)
    {
      array[0] = (GLubyte)(color >> 24);
      array[1] = (GLubyte)(color >> 16);
      array[2] = (GLubyte)(color >> 8);
      array[3] = (GLubyte)color;
    }

    /*
     * Accessors
     */

    //! Get display Width as integer
    int DisplayWidthAsInt() const { return mDisplayWidth; }
    //! Get display Height as integer
    int DisplayHeightAsInt() const { return mDisplayHeight; }
    //! Get display Width as float
    float DisplayWidthAsFloat() const { return mDisplayWidthFloat; }
    //! Get display Height as float
    float DisplayHeightAsFloat() const { return mDisplayHeightFloat; }

    //! Screen ratio (height) - Return pixel height from ratio as int
    int RatioAsInt(float ratio) const { return (int)(mDisplayHeightFloat * ratio + 0.6); }
    //! Screen ratio (height) - Return pixel height from ratio as float
    float RatioAsFloat(float ratio) const { return (mDisplayHeightFloat * ratio + 0.6); }

    // Is small resolution?
    bool IsSmallResolution() const { return mDisplayWidth <= 480 || mDisplayHeight <= 320; }

    //! Check if the Renderer is properly initialized
    bool Initialized() const { return mViewPortInitialized; }

    SDL_Window* GetWindow() { return mSdlWindow; }

    SDL_Surface* GetWindowSurface() { return SDL_GetWindowSurface(mSdlWindow); }
};
