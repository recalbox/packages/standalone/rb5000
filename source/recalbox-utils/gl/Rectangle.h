//
// Created by bkg2k on 12/09/2020.
//
#pragma once

#include "Vector2f.h"
#include "Math.h"

class Rectangle
{
  public:
    Rectangle()
      : mX(0)
      , mY(0)
      , mW(0)
      , mH(0)
    {
    }

    explicit Rectangle(const Vector2f& size)
      : mX(0)
      , mY(0)
      , mW(size.x())
      , mH(size.y())
    {
    }

    Rectangle(float width, float height)
      : mX(0)
      , mY(0)
      , mW(width)
      , mH(height)
    {
    }

    Rectangle(const Vector2f& topleft, const Vector2f& widthheight)
      : mX(topleft.x())
      , mY(topleft.y())
      , mW(widthheight.x())
      , mH(widthheight.y())
    {
    }

    Rectangle(float x, float y, float width, float height)
      : mX(x)
      , mY(y)
      , mW(width)
      , mH(height)
    {
    }

    float Left()   const { return mX; }
    float Top()    const { return mY; }
    float Right()  const { return mX + mW; }
    float Bottom() const { return mY + mH; }
    float Width()  const { return mW; }
    float Height() const { return mH; }

    Rectangle& SetLeft(float x)
    {
      mX = x;
      return *this;
    }

    Rectangle& SetTop(float y)
    {
      mY = y;
      return *this;
    }

    Rectangle& SetWidth(float w)
    {
      mW = w;
      return *this;
    }

    Rectangle& SetHeight(float h)
    {
      mH = h;
      return *this;
    }

    Rectangle& Set(float x, float y, float w, float h)
    {
      mX = x;
      mY = y;
      mW = w;
      mH = h;
      return *this;
    }

    Rectangle& SetCoordinates(float x, float y)
    {
      mX = x;
      mY = y;
      return *this;
    }

    Rectangle& SetSize(float w, float h)
    {
      mW = w;
      mH = h;
      return *this;
    }

    Rectangle SizeFactor(float r)
    {
      mW *= r;
      mH *= r;
      return *this;
    }

    Rectangle& Contract(const Vector2f& by)
    {
      mX += by.x();
      mY += by.y();
      mW -= by.x() * 2;
      mH -= by.y() * 2;

      if (mW < 0) { mX += mW / 2; mW = 0; }
      if (mH < 0) { mY += mH / 2; mH = 0; }

      return *this;
    }

    Rectangle& Expand(const Vector2f& by)
    {
      mX -= by.x();
      mY -= by.y();
      mW += by.x() * 2;
      mH += by.y() * 2;

      if (mW < 0) { mX += mW / 2; mW = 0; }
      if (mH < 0) { mY += mH / 2; mH = 0; }

      return *this;
    }

    Rectangle& Contract(float deltaX, float deltaY)
    {
      mX += deltaX;
      mY += deltaY;
      mW -= deltaX * 2;
      mH -= deltaY * 2;

      if (mW < 0) { mX += mW / 2; mW = 0; }
      if (mH < 0) { mY += mH / 2; mH = 0; }

      return *this;
    }

    Rectangle& Expand(float deltaX, float deltaY)
    {
      mX -= deltaX;
      mY -= deltaY;
      mW += deltaX * 2;
      mH += deltaY * 2;

      if (mW < 0) { mX += mW / 2; mW = 0; }
      if (mH < 0) { mY += mH / 2; mH = 0; }

      return *this;
    }

    Rectangle& Move(const Vector2f& by)
    {
      mX += by.x();
      mY += by.y();
      return *this;
    }

    Rectangle& Move(float deltaX, float deltaY)
    {
      mX += deltaX;
      mY += deltaY;
      return *this;
    }

    Rectangle& MoveLeft(float delta)
    {
      mX += delta;
      if ((mW -= delta) < 0.0f) mW = 0.0f;
      return *this;
    }

    Rectangle& MoveTop(float delta)
    {
      mY += delta;
      if ((mH -= delta) < 0.0f) mH = 0.0f;
      return *this;
    }

    Rectangle& MoveRight(float delta)
    {
      if ((mW += delta) < 0.0f) mW = 0.0f;
      return *this;
    }

    Rectangle& MoveBottom(float delta)
    {
      if ((mH += delta) < 0.0f) mH = 0.0f;
      return *this;
    }

    Rectangle& ResetReference()
    {
      mX = mY = 0;
      return *this;
    }

    Rectangle& Reset()
    {
      mX = mY = mW = mH = 0;
      return *this;
    }

    bool operator ==(const Rectangle& r) const
    {
      return ((mX == r.mX) && (mY == r.mY) && (mW == r.mW) && (mH == r.mH));
    }

    bool operator !=(const Rectangle& r) const
    {
      return ((mX != r.mX) || (mY != r.mY) || (mW != r.mW) || (mH != r.mH));
    }

    Rectangle& FlipY()
    {
      mY += mH;
      mH = -mH;
      return *this;
    }

    Rectangle& FlipOnScreen(int screenHeight)
    {
      mY = (float)screenHeight - mY - mH;
      return *this;
    }

    Rectangle& Intersect(const Rectangle& with)
    {
      if (mX < with.mX) { mW -= with.mX - mX; mX = with.mX; if (mW < 0) mW = 0; }
      if (mX + mW > with.mX + with.mW) { mW = with.mX + with.mW - mX; if (mW < 0) mW = 0; }
      if (mY < with.mY) { mH -= with.mY - mY; mY = with.mY; if (mH < 0) mH = 0; }
      if (mY + mH > with.mY + with.mH) { mH = with.mY + with.mH - mY; if (mH < 0) mH = 0; }
      return *this;
    }

    bool IsIn(float x, float y) const
    {
      return x >= mX && x < (mX + mW) && y >= mY && y < (mY + mH);
    }

    Rectangle& Scale(float xratio, float yratio, float xreference, float yreference)
    {
      float xR = mX + mW * xreference;
      float yR = mY + mH * yreference;
      mX = xR - ((xR - mX) * xratio);
      mY = yR - ((yR - mY) * yratio);
      mW *= xratio;
      mH *= yratio;
      return *this;
    }

    Rectangle& Validate()
    {
      if (mW < 0) mW = 0;
      if (mH < 0) mH = 0;
      return *this;
    }

    Rectangle& Round()
    {
      float r = Math::round(mX + mW);
      mX = Math::round(mX);
      mW = r - mX;
      float b = Math::round(mY + mH);
      mY = Math::round(mY);
      mH = b - mY;
      return *this;
    }

    Rectangle& Ceil()
    {
      float r = Math::ceil(mX + mW);
      mX = Math::ceil(mX);
      mW = r - mX;
      float b = Math::ceil(mY + mH);
      mY = Math::ceil(mY);
      mH = b - mY;
      return *this;
    }

    bool IsValid() const { return mW > 0 && mH > 0; }

    float XCenter() const { return mX + mW / 2.f; }

    float YCenter() const { return mY + mH / 2.f; }

  private:
    float mX;
    float mY;
    float mW;
    float mH;
};
