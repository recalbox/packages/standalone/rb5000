#pragma once

//the Makefile defines one of these:
//#define USE_OPENGL_ES
//#define USE_OPENGL_DESKTOP

#ifdef USE_OPENGL_ES
  #include <GLES/gl.h>
#endif

#ifdef USE_OPENGL_DESKTOP
  #define GL_GLEXT_PROTOTYPES
  #include <SDL2/SDL_opengl.h>
#endif
