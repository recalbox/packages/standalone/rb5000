#include "Math.h"
#include "Renderer.h"
#include "../Log.h"

#ifdef USE_OPENGL_ES
  #define glOrtho glOrthof
#endif

float Renderer::mLastXTranslation = 0.f;
float Renderer::mLastYTranslation = 0.f;

Renderer::Renderer()
  : mSdlWindow(nullptr)
  , mSdlGLContext(nullptr)
  , mDisplayWidth(0)
  , mDisplayHeight(0)
  , mDisplayWidthFloat(0.0f)
  , mDisplayHeightFloat(0.0f)
  , mInitialCursorState(false)
  , mViewPortInitialized(false)
{
}

Renderer::~Renderer()
{
  Finalize();
}

bool Renderer::CreateSdlSurface()
{
  { LOG(LogInfo) << "[Renderer] Creating surface..."; }

  if (SDL_Init(SDL_INIT_VIDEO) != 0)
  {
    { LOG(LogError) << "[Renderer] Error initializing SDL!\n	" << SDL_GetError(); }
    return false;
  }

  //hide mouse cursor
  mInitialCursorState = SDL_ShowCursor(0) == 1;

  SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
  SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
  SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

  // multisample anti-aliasing
  //SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
  //SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 2);

  #ifdef USE_OPENGL_ES
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 1);
  #endif

  SDL_DisplayMode dispMode;
  SDL_GetDesktopDisplayMode(0, &dispMode);
  if (mDisplayWidth <= 0)  mDisplayWidth = dispMode.w;
  if (mDisplayHeight <= 0) mDisplayHeight = dispMode.h;
  mDisplayWidthFloat = (float)mDisplayWidth;
  mDisplayHeightFloat = (float)mDisplayHeight;
  { LOG(LogInfo) << "[Renderer] Resolution: " << mDisplayWidth << ',' << mDisplayHeight; }

  mSdlWindow = SDL_CreateWindow("RB5000 by Recalbox",
                                SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
                                mDisplayWidth,
                                mDisplayHeight,
                                SDL_WINDOW_OPENGL | (mWindowed ? 0 : SDL_WINDOW_FULLSCREEN));

  if (mSdlWindow == nullptr)
  {
    { LOG(LogError) << "[Renderer] Error creating SDL window!\n\t" << SDL_GetError(); }
    return false;
  }
  { LOG(LogInfo) << "[Renderer] Created window successfully."; }

  mSdlGLContext = SDL_GL_CreateContext(mSdlWindow);

  // vsync
  { LOG(LogInfo) << "[Renderer] Activating vertical sync'"; }
  // SDL_GL_SetSwapInterval(0) for immediate updates (no vsync, default),
  // 1 for updates synchronized with the vertical retrace,
  // or -1 for late swap tearing.
  // SDL_GL_SetSwapInterval returns 0 on success, -1 on error.
  // if vsync is requested, try late swap tearing; if that doesn't work, try normal vsync
  // if that doesn't work, report an error
  /*if (SDL_GL_SetSwapInterval(-1) == 0) LOG(LogInfo) << "Adaptative VSync' activated.";
  else*/ if (SDL_GL_SetSwapInterval(1) == 0) { LOG(LogInfo) << "[Renderer] Normal VSync' activated."; }
  else { LOG(LogWarning) << "[Renderer] Tried to enable vsync, but failed! (" << SDL_GetError() << ")"; }

  return true;
}

void Renderer::SwapBuffers()
{
  SDL_GL_SwapWindow(mSdlWindow);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Renderer::DestroySdlSurface()
{
  SDL_GL_DeleteContext(mSdlGLContext);
  mSdlGLContext = nullptr;

  SDL_DestroyWindow(mSdlWindow);
  mSdlWindow = nullptr;

  // show mouse cursor
  SDL_ShowCursor(mInitialCursorState ? 1 : 0);

  SDL_Quit();
}

bool Renderer::Initialize(int& w, int& h, bool windowed)
{
  mWindowed = windowed;
  if (w > 0) mDisplayWidth = w;
  if (h > 0) mDisplayHeight = h;

  bool createdSurface = CreateSdlSurface();
  if (!createdSurface) return mViewPortInitialized = false;

  glViewport(0, 0, mDisplayWidth, mDisplayHeight);

  glMatrixMode(GL_PROJECTION);
  glOrtho(0, mDisplayWidth, mDisplayHeight, 0, -1.0, 1.0);
  glMatrixMode(GL_MODELVIEW);
  glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

  w = mDisplayWidth;
  h = mDisplayHeight;

  return mViewPortInitialized = true;
}

void Renderer::Finalize()
{
  DestroySdlSurface();
}

GLuint Renderer::CreateGLTexture()
{
  GLuint id = 0;

  glGenTextures(1, &id);
  glBindTexture(GL_TEXTURE_2D, id);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  return id;
}

void Renderer::FilterTexture(GLuint id, bool smooth)
{
  glBindTexture(GL_TEXTURE_2D, id);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  if (smooth)
  {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  }
  else
  {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  }
}

void Renderer::DestroyGLTexture(GLuint id)
{
  glDeleteTextures(1, &id);
}

void Renderer::BuildGLColorArray(GLubyte* ptr, Colors::ColorARGB color, int vertCount)
{
  unsigned int colorGl = 0;
  ColorToByteArray((GLubyte*) &colorGl, color);
  for (int i = vertCount; --i >= 0; )
    ((GLuint*) ptr)[i] = colorGl;
}

void Renderer::DrawRectangle(const Rectangle& area, Colors::ColorARGB color, GLenum blend_sfactor, GLenum blend_dfactor)
{
  DrawRectangle(Math::roundi(area.Left()), Math::roundi(area.Top()),
                Math::roundi(area.Width()), Math::roundi(area.Height()),
                color, blend_sfactor, blend_dfactor);
}

void
Renderer::DrawRectangle(float x, float y, float w, float h, Colors::ColorARGB color, GLenum blend_sfactor, GLenum blend_dfactor)
{
  DrawRectangle(Math::roundi(x), Math::roundi(y), Math::roundi(w), Math::roundi(h), color, blend_sfactor, blend_dfactor);
}

void Renderer::DrawRectangle(int x, int y, int w, int h, Colors::ColorARGB color, GLenum blend_sfactor, GLenum blend_dfactor)
{
  #ifdef USE_OPENGL_ES
  GLshort points[12];
  #else
  GLint points[12];
  #endif

  points[0] = x;
  points[1] = y;
  points[2] = x;
  points[3] = y + h;
  points[4] = x + w;
  points[5] = y;

  points[6] = x + w;
  points[7] = y;
  points[8] = x;
  points[9] = y + h;
  points[10] = x + w;
  points[11] = y + h;

  GLubyte colors[6 * 4];
  BuildGLColorArray(colors, color, 6);

  glEnable(GL_BLEND);
  glBlendFunc(blend_sfactor, blend_dfactor);
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);

  #ifdef USE_OPENGL_ES
  glVertexPointer(2, GL_SHORT, 0, points);
  #else
  glVertexPointer(2, GL_INT, 0, points);
  #endif
  glColorPointer(4, GL_UNSIGNED_BYTE, 0, colors);

  glDrawArrays(GL_TRIANGLES, 0, 6);

  glDisable(GL_BLEND);
  glDisableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);
}

void Renderer::SetMatrix(const Transform4x4f& transform)
{
  mLastXTranslation = transform.r3().x();
  mLastYTranslation = transform.r3().y();
  glLoadMatrixf((float*) &transform);
}

Renderer::Error Renderer::UploadRGBA(GLuint id, int width, int height, const void* data)
{
  glBindTexture(GL_TEXTURE_2D, id);
  if (glGetError() != GL_NO_ERROR) return Error::NoResource;

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
  if (glGetError() == GL_OUT_OF_MEMORY) return Error::OutOfGPUMemory;

  return Error::NoError;
}

Renderer::Error Renderer::UploadRGB565(GLuint id, int width, int height, const void* data)
{
  EmptyErrors();

  glBindTexture(GL_TEXTURE_2D, id);
  if (CheckErrors()) return Error::NoResource;

  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, data);
  if (CheckErrors()) return Error::OutOfGPUMemory;

  return Error::NoError;
}

Renderer::Error Renderer::UploadRGB565Part(GLuint id, int x, int y, int width, int height, const void* data)
{
  EmptyErrors();

  glBindTexture(GL_TEXTURE_2D, id);
  if (CheckErrors()) return Error::NoResource;

  glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, width, height, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, data);
  if (CheckErrors()) return Error::OutOfGPUMemory;

  return Error::NoError;
}

void Renderer::DrawLines(const Vector2f coordinates[], const Colors::ColorARGB colors[], int count)
{
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);

  glVertexPointer(2, GL_FLOAT, 0, &coordinates);
  glColorPointer(4, GL_UNSIGNED_BYTE, 0, &colors);

  glDrawArrays(GL_LINES, 0, count);

  glDisable(GL_BLEND);
  glDisableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);
}

void Renderer::DrawTexturedTriangles(GLuint id, const Vertex vertices[], const GLubyte colors[], int count, bool tiled)
{
  if (id != 0)
    glBindTexture(GL_TEXTURE_2D, id);

  glEnable(GL_TEXTURE_2D);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  if (tiled)
  {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  }

  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);
  glEnableClientState(GL_COLOR_ARRAY);

  glVertexPointer(2, GL_FLOAT, sizeof(Vertex), &vertices[0].Target);
  glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), &vertices[0].Source);
  glColorPointer(4, GL_UNSIGNED_BYTE, 0, colors);

  glDrawArrays(GL_TRIANGLES, 0, count);

  glDisableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  glDisableClientState(GL_COLOR_ARRAY);

  if (tiled)
  {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  }

  glDisable(GL_TEXTURE_2D);
  glDisable(GL_BLEND);
}

void Renderer::DrawTexturedTriangles(GLuint id, const Vertex vertices[], Colors::ColorARGB color, int count, bool tiled)
{
  if (id != 0)
    glBindTexture(GL_TEXTURE_2D, id);

  glEnable(GL_TEXTURE_2D);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  if (tiled)
  {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  }

  glColor4ub((GLubyte)(color >> 16),
             (GLubyte)(color >> 8),
             (GLubyte)(color >> 0),
             (GLubyte)(color >> 24));

  glEnableClientState(GL_VERTEX_ARRAY);
  glEnableClientState(GL_TEXTURE_COORD_ARRAY);

  glVertexPointer(2, GL_FLOAT, sizeof(Vertex), &vertices[0].Target);
  glTexCoordPointer(2, GL_FLOAT, sizeof(Vertex), &vertices[0].Source);

  glDrawArrays(GL_TRIANGLES, 0, count);

  glDisableClientState(GL_VERTEX_ARRAY);
  glDisableClientState(GL_TEXTURE_COORD_ARRAY);

  glColor4ub(0xFF, 0xFF, 0xFF, 0xFF);

  if (tiled)
  {
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  }

  glDisable(GL_TEXTURE_2D);
  glDisable(GL_BLEND);
}

bool Renderer::CheckErrors()
{
  bool result = false;
  for(bool exit = false; !exit; )
    switch(glGetError())
    {
      case GL_NO_ERROR: exit = true; break;
      case GL_INVALID_ENUM: LOG(LogError) << "[GLError] " << "An unacceptable value is specified for an enumerated argument. The offending command is ignored and has no other side effect than to set the error flag."; result = true; break;
      case GL_INVALID_VALUE: LOG(LogError) << "[GLError] " << "A numeric argument is out of range. The offending command is ignored and has no other side effect than to set the error flag."; result = true; break;
      case GL_INVALID_OPERATION: LOG(LogError) << "[GLError] " << "The specified operation is not allowed in the current state. The offending command is ignored and has no other side effect than to set the error flag."; result = true; break;
      //case GL_INVALID_FRAMEBUFFER_OPERATION: LOG(LogError) << "[GLError] " << "The framebuffer object is not complete. The offending command is ignored and has no other side effect than to set the error flag."; result = true; break;
      case GL_OUT_OF_MEMORY: LOG(LogError) << "[GLError] " << "There is not enough memory left to execute the command. The state of the GL is undefined, except for the state of the error flags, after this error is recorded."; result = true; break;
      case GL_STACK_UNDERFLOW: LOG(LogError) << "[GLError] " << "An attempt has been made to perform an operation that would cause an internal stack to underflow."; result = true; break;
      case GL_STACK_OVERFLOW: LOG(LogError) << "[GLError] " << "An attempt has been made to perform an operation that would cause an internal stack to overflow." ; result = true; break;
      default: LOG(LogError) << "[GLError] " << "Unknown error." ; result = true; break;
    }

  return result;
}

void Renderer::EmptyErrors()
{
  while(glGetError() != GL_NO_ERROR);
}

