//
// Created by bkg2k on 10/10/22.
//

#include "Printer.h"
#include "recalbox-utils/Files.h"

void Printer::PrintByte(byte data)
{
  if (!mConfiguration.PrinterFileName().empty())
    Files::AppendToFile(mConfiguration.PrinterFileName(), &data, 1);
}
