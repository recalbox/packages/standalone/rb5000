//
// Created by bkg2k on 12/10/22.
//

#pragma once

#include "Configuration.h"
#include "../Z80/Z80.h"

class Joystick
{
  public:
    /*!
     * @brief Constructor
     * @param configuration Configuration
     */
    explicit Joystick(const Configuration& configuration);

    //! Destructor
    ~Joystick();

    //! Initialize Joysticks
    void Initialize();

    /*!
     * @brief Process all joystick events
     * @param event Joystick event
     */
    void ProcessJoystickEvent(const SDL_Event& event);

    //! Quit?
    [[nodiscard]] bool QuitRequested() const { return mHotkey && mStart; }

    //! Joystick 0 port value
    [[nodiscard]] byte JoystickPort(int joystick) const { return mPorts[joystick % Configuration::sJoystickCount]; }

  private:
    //! Configuration reference
    const Configuration& mConfiguration;

    //! Joysticks
    SDL_Joystick* mJoysticks[Configuration::sJoystickCount];
    //! Joystick port data
    byte mPorts[Configuration::sJoystickCount];
    //! Hotkey
    bool mHotkey;
    //! Start
    bool mStart;
};
