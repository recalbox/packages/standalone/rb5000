//
// Created by bkg2k on 10/10/22.
//

#pragma once


#include "Configuration.h"
#include "Tape.h"
#include "KeyboardMatrix.h"
#include "Video.h"
#include "Printer.h"
#include "Engine.h"
#include "recalbox-utils/Mutex.h"
#include "EF9345.h"

class Main
{
  public:
    //! Constructor
    Main();

    //! Run emulation until exiting
    void Run(int argc, char *argv[]);

  private:
    //! Configuration
    Configuration mConfiguration;
    //! Emulation engine
    Engine mEngine;

    /*!
     * @brief Run SDL loop & screen refresh in main thread
     */
    void SDLLoop();
};
