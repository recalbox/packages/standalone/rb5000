//
// Created by bkg2k on 12/10/22.
//
#pragma once

#include "Video.h"

class EF9345
{
  public:
    /*!
     * @brief Constructor
     * @param video Video output
     */
    explicit EF9345(Video& video);

    /*!
     * @brief Initialize EF9345
     */
    void Initialize();

    /*!
     * @brief Read
     * @param r Register
     * @return Value
     */
    byte Read(byte r);

    /*!
     * @brief Write
     * @param r Register
     * @param c Data
     */
    void Write(byte r, byte c);

    /*!
     * @brief Blink!
     */
    void Blinking() { mBlinckState = 1 - mBlinckState;}

    /*!
     * @brief Dsiplay a character line
     * @param n Line number
     */
    void Displayline(int n);

    /*!
     * @brief Consume the given amount of cycles
     * @param cycles Cycle count
     */
    void ConsumeCycles(int cycles) { if(mCycles > 0) mCycles -= cycles; }

  private:
    typedef void (EF9345::*MakeCharMethod)(int x, int y);

    //! Video reference
    Video& mVideo;

    //! Internal RAM
    byte mRAM[0x4000];
    //! Internal rom
    byte mROM[0x2000];
    //! Internal registers
    byte mRegs[8];
    //! Status register
    byte mStatus;
    //! Cycle counter
    int  mCycles;
    //! Pixel repeat
    int  mXRepeat;
    //! Indirect register TGS
    byte mTGS;
    //! Indirect register MAT
    byte mMAT;
    //! Indirect register PAT
    byte mPAT;
    //! Indirect register DOR
    byte mDOR;
    //! Indirect register ROR
    byte mROR;
    //! Border charactre array
    byte mBorders[80];
    //! Current block
    byte* mBlock;
    //! Curren character matrix
    const byte *mMatrix;
    //! Fonts
    const byte* mFonts[16];
    //! Blink state
    int mBlinckState;
    //! Current Quadran
    int mQuadran;
    //! Last Quadrans
    int mLastQuadrans[40];
    //! Latched color C0
    int mLatchC0;
    //! Latched conceal attribute
    int mLatchM;
    //! Latched insert attribute
    int mLatchI;
    //! Latched underline attribute
    int mLatchU;
    //! Unimplemented Error count
    int mErrorCount;
    //! Current make char method
    MakeCharMethod mMakeChar;

    /*!
     * @brief Emit log on unimplemented stuff
     * @param message
     */
    void Todo(const char* message, int code) const;

    /*!
     * @brief Set graphic mode
     */
    void SetMode();

    /*!
     * @brief Initialize ROM & Fonts
     */
    void InitializeFonts();

    /*!
     * @brief Get ram index
     * @param r Register
     * @return Ram index
     */
    int Indexram(int r);
    /*!
     * @brief Get rom index
     * @param r Register
     * @return Rom index
     */
    int Indexrom(int r);
    /*!
     * @brief Get block index
     * @param x X coordinate
     * @param y Y coordinate
     * @return Block index
     */
    int Indexbloc(int x, int y);

    /*!
     * @brief Increment X in the given register
     * @param r Register
     */
    void Incrx(int r);

    /*!
     * @brief Increment Y in the given register
     * @param r Register
     */
    void Incry(int r);

    /*!
     * @brief Calculate Quadran to display
     * @param x X corrdinate
     * @param attrib Attribute
     */
    void Cadran(int x, int attrib);

    /*!
     * @brief Zoom character part
     * @param pix Pixels
     * @param n Zone to zoom
     */
    static void Zoom(byte *pix, int n);

    /*!
     * @brief Display bi-color character in 40 column mode
     * @param ibloc Block
     * @param x X coordinate
     * @param y Y coordinate
     * @param c0 Color 0
     * @param c1 Color 1
     * @param insert Insert attribute
     * @param flash Flash attribute
     * @param conceal Conseal attribute
     * @param negative Negative?
     * @param underline Underline?
     */
    void Bichrome40(int ibloc, int x, int y, int c0, int c1, int insert,
                    int flash, int conceal, int negative, int underline);

    /*!
     * @brief Display 4-color charcter in 40 column mode
     * @param c Character code
     * @param b Attributes + Set number
     * @param a Color index
     * @param x X coordinate
     * @param y Y coordinate
     */
    void Quadrichrome40(int c, int b, int a, int x, int y);

    /*!
     * @brief Display bi-color character in 80 collumn mode
     * @param c Character code
     * @param a Attributes
     * @param x X coordinate
     * @param y Y coordinate
     */
    void Bichrome80(int c, int a, int x, int y);

    /*!
     * @brief Display 16bit character in 40 column
     * @param x X coordinate
     * @param y Y coordinate
     */
    void Makechar16x40(int x, int y);

    /*!
     * @brief Display 24bit character in 40 column
     * @param x X coordinate
     * @param y Y coordinate
     */
    void Makechar24x40(int x, int y);

    /*!
     * @brief Display variable character in 40 column
     * @param x X coordinate
     * @param y Y coordinate
     */
    void Makecharvar40(int x, int y);

    /*!
     * @brief Display variable character in 80 column
     * @param x X coordinate
     * @param y Y coordinate
     */
    void Makechar8x80(int x, int y);

    /*!
     * @brief Display 12bit character in 40 column
     * @param x X coordinate
     * @param y Y coordinate
     */
    void Makechar12x80(int x, int y);

    void OCTwrite(int r, int i);
    void OCTread(int r, int i);
    void KRGwrite(int i);
    void KRGread(int i);
    void KRFwrite(int i);
    void KRFread(int i);
    void KRLwrite(int i);
    void KRLread(int i);

    void MVB(int n, int r1, int r2, int s);
    void INDwrite(int x);
    void INDread(int x);
    void INY();

    /*!
     * @brief Execute instruction
     * @param opcode Opcode to execute
     */
    void Exec(int opcode);
};
