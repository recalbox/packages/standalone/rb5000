//
// Created by bkg2k on 09/10/22.
//

#include <cstring>
#include "Configuration.h"
#include "recalbox-utils/Files.h"
#include "recalbox-utils/Log.h"

int Configuration::sCPUSpeedFromIndex[Configuration::sCPUSpeedCount] =
{
    50, // 0
   250, // 1
   500, // 2
   750, // 3
   900, // 4
  1000, // 5 - Default speed
  1250, // 6
  1500, // 7
  2000, // 8
  5000, // 9
};

void Configuration::LoadConfiguration(int argc, char* argv[])
{
  String fileName = GetFileFromCommandLine(argc, argv);
  ReadFromFile(fileName);
  ReadFromCommandLine(argc, argv);
  // Check
  if (mCPUSpeedIndex < 0) mCPUSpeedIndex = 0;
  if (mCPUSpeedIndex >= sCPUSpeedCount) mCPUSpeedIndex = sCPUSpeedCount - 1;
  if (mIntegerScale) mSmooth = false;
}

String Configuration::GetFileFromCommandLine(int argc, char** argv)
{
  for(int i = 0; i < argc - 1; i++)
    if (strcmp(argv[i], "-c") == 0 || strcmp(argv[i], "--configfile") == 0)
      return argv[i + 1];
  return "./.vg5000.cfg";
}

void Configuration::ReadFromCommandLine(int argc, char** argv)
{
  for(int i = 1; i < argc - 1; i++)
  {
    if      (strcmp(argv[i], "-p"   ) == 0 || strcmp(argv[i], "--printerfile"     ) == 0) mPrinterFileName           = argv[++i];
    else if (strcmp(argv[i], "-t"   ) == 0 || strcmp(argv[i], "--tapefile"        ) == 0) mTapeFileName              = argv[++i];
    else if (strcmp(argv[i], "-a"   ) == 0 || strcmp(argv[i], "--tapeautorun"     ) == 0) mTapeAutoRun               = true;
    else if (strcmp(argv[i], "-wr"  ) == 0 || strcmp(argv[i], "--tapewritable"    ) == 0) mTapeWritable              = true;
    else if (strcmp(argv[i], "-s"   ) == 0 || strcmp(argv[i], "--speedindex"      ) == 0) mCPUSpeedIndex             = String(argv[++i]).AsInt();
    else if (strcmp(argv[i], "-j0"  ) == 0 || strcmp(argv[i], "--joystick0index"  ) == 0) mJoystickIndex[0]          = String(argv[++i]).AsInt();
    else if (strcmp(argv[i], "-j1"  ) == 0 || strcmp(argv[i], "--joystick1index"  ) == 0) mJoystickIndex[1]          = String(argv[++i]).AsInt();
    else if (strcmp(argv[i], "-j0b0") == 0 || strcmp(argv[i], "--joystick0button0") == 0) mJoystickButtons[0][0]     = String(argv[++i]).AsInt();
    else if (strcmp(argv[i], "-j0b1") == 0 || strcmp(argv[i], "--joystick0button1") == 0) mJoystickButtons[0][1]     = String(argv[++i]).AsInt();
    else if (strcmp(argv[i], "-j1b0") == 0 || strcmp(argv[i], "--joystick1button0") == 0) mJoystickButtons[1][0]     = String(argv[++i]).AsInt();
    else if (strcmp(argv[i], "-j1b1") == 0 || strcmp(argv[i], "--joystick1button1") == 0) mJoystickButtons[1][1]     = String(argv[++i]).AsInt();
    else if (strcmp(argv[i], "-j0u" ) == 0 || strcmp(argv[i], "--joystick0up"     ) == 0) mJoystickDirectionUp[0]    = String(argv[++i]).AsInt();
    else if (strcmp(argv[i], "-j0r" ) == 0 || strcmp(argv[i], "--joystick0right"  ) == 0) mJoystickDirectionRight[0] = String(argv[++i]).AsInt();
    else if (strcmp(argv[i], "-j0d" ) == 0 || strcmp(argv[i], "--joystick0down"   ) == 0) mJoystickDirectionDown[0]  = String(argv[++i]).AsInt();
    else if (strcmp(argv[i], "-j0l" ) == 0 || strcmp(argv[i], "--joystick0left"   ) == 0) mJoystickDirectionLeft[0]  = String(argv[++i]).AsInt();
    else if (strcmp(argv[i], "-hk"  ) == 0 || strcmp(argv[i], "--joystick0hotkey" ) == 0) mJoystickHotkey            = String(argv[++i]).AsInt();
    else if (strcmp(argv[i], "-st"  ) == 0 || strcmp(argv[i], "--joystick0start"  ) == 0) mJoystickStart             = String(argv[++i]).AsInt();
    else if (strcmp(argv[i], "-w"   ) == 0 || strcmp(argv[i], "--width"           ) == 0) mWindowWidth               = String(argv[++i]).AsInt();
    else if (strcmp(argv[i], "-h"   ) == 0 || strcmp(argv[i], "--height"          ) == 0) mWindowHeight              = String(argv[++i]).AsInt();
    else if (strcmp(argv[i], "-o"   ) == 0 || strcmp(argv[i], "--smooth"          ) == 0) mSmooth                    = true;
    else if (strcmp(argv[i], "-i"   ) == 0 || strcmp(argv[i], "--integerscale"    ) == 0) mIntegerScale              = true;
    else if (strcmp(argv[i], "-f"   ) == 0 || strcmp(argv[i], "--fullscreen"      ) == 0) mFullscreen                = true;
    else if (strcmp(argv[i], "-c"   ) == 0 || strcmp(argv[i], "--configfile"      ) == 0) ++i;
    else LOG(LogError) << "[Configuration] Unknown option " << argv[i];
  }
}

void Configuration::ReadFromFile(const String& fileName)
{
  String content = Files::LoadFile(fileName);
  if (!content.empty())
    LOG(LogInfo) << "[Configuration] Configuration file " << fileName << " loaded.";
  String::List lines = content.Split('\n');
  String key, value;
  for(const String& orgLine : lines)
    if (orgLine.ToTrim().Extract('=', key, value, true))
    {
      if      (key == "printer.filename"      ) mPrinterFileName           = value;
      else if (key == "tape.filename"         ) mTapeFileName              = value;
      else if (key == "tape.autorun"          ) mTapeAutoRun               = value.AsBool();
      else if (key == "tape.writable"         ) mTapeWritable              = value.AsBool();
      else if (key == "cpu.speedindex"        ) mCPUSpeedIndex             = value.AsInt();
      else if (key == "joystick.left.index"   ) mJoystickIndex[0]          = value.AsInt();
      else if (key == "joystick.right.index"  ) mJoystickIndex[1]          = value.AsInt();
      else if (key == "joystick.left.button1" ) mJoystickButtons[0][0]     = value.AsInt();
      else if (key == "joystick.left.button2" ) mJoystickButtons[0][1]     = value.AsInt();
      else if (key == "joystick.left.hotkey"  ) mJoystickHotkey            = value.AsInt();
      else if (key == "joystick.left.start"   ) mJoystickStart             = value.AsInt();
      else if (key == "joystick.right.button1") mJoystickButtons[1][0]     = value.AsInt();
      else if (key == "joystick.right.button2") mJoystickButtons[1][1]     = value.AsInt();
      else if (key == "joystick.left.up"      ) mJoystickDirectionUp[0]    = value.AsInt();
      else if (key == "joystick.left.right"   ) mJoystickDirectionRight[0] = value.AsInt();
      else if (key == "joystick.left.down"    ) mJoystickDirectionDown[0]  = value.AsInt();
      else if (key == "joystick.left.left"    ) mJoystickDirectionLeft[0]  = value.AsInt();
      else if (key == "joystick.right.up"     ) mJoystickDirectionUp[1]    = value.AsInt();
      else if (key == "joystick.right.right"  ) mJoystickDirectionRight[1] = value.AsInt();
      else if (key == "joystick.right.down"   ) mJoystickDirectionDown[1]  = value.AsInt();
      else if (key == "joystick.right.left"   ) mJoystickDirectionLeft[1]  = value.AsInt();
      else if (key == "renderer.width"        ) mWindowWidth               = value.AsInt();
      else if (key == "renderer.height"       ) mWindowHeight              = value.AsInt();
      else if (key == "renderer.smooth"       ) mSmooth                    = value.AsBool();
      else if (key == "renderer.integerscale" ) mIntegerScale              = value.AsBool();
      else if (key == "renderer.fullscreen"   ) mFullscreen                = value.AsBool();
    }
}
