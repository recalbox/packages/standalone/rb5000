//
// Created by bkg2k on 10/10/22.
//
#pragma once

#include "recalbox-utils/String.h"
#include "Configuration.h"
#include "recalbox-utils/Files.h"
#include "../Z80/Z80.h"

class Tape
{
  public:
    /*!
     * @brief Default constructor
     * @param configuration Configuration reference
     */
    explicit Tape(const Configuration& configuration)
      : mConfiguration(configuration)
      , mContent()
      , mIndex(0)
      , mHasBeenWritten(false)
    {
      LoadTape();
    }

    //! Default destructor
    ~Tape()
    {
      if (mHasBeenWritten && !mConfiguration.TapeFileName().empty())
        Files::SaveFile(mConfiguration.TapeFileName(), mContent);
    }

    /*!
     * @brief Load tape
     */
    void LoadTape()
    {
      if (!mConfiguration.TapeFileName().empty())
        mContent = Files::LoadFile(mConfiguration.TapeFileName());
    }

    /*!
     * @brief Tape is loaded and has content?
     * @return True if a tape is loaded and has content, false otherwise
     */
    bool IsTapeLoaded() const { return !mContent.empty(); }

    //! Get filename
    [[nodiscard]] const String& FileName() const { return mConfiguration.TapeFileName(); }
    //! Get content
    [[nodiscard]] const String& Content() const { return mContent; }
    //! Get current index
    [[nodiscard]] int Index() const { return mIndex; }
    //! Get max index
    [[nodiscard]] int Size() const { return (int)mContent.size(); }

    /*!
     * @brief Read byte from K7
     * @param read output value
     * @return True if a byte has been successfully read, false otherwise
     */
    bool ReadByte(byte& read);

    /*!
     * @brief Write byte to the current tape
     * @param written
     * @return True if the operation is successfully, false if there is no tape or the tape is write protected
     * or in case of error
     */
    bool WriteByte(byte written);

    /*!
     * @brief Rewind tape
     * @param amount Amount to byte to rewind
     */
    void Rewind(int amount);

    /*!
     * @brief Fast forward tape
     * @param amount Amount to byte to fast forward
     */
    void FastForward(int amount);

  private:
    //! Configuration reference
    const Configuration& mConfiguration;

    //! Tape file content
    String mContent;
    //! Current index
    int mIndex;
    //! K7 has been written?
    bool mHasBeenWritten;
};
