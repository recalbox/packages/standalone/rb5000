//
// Created by bkg2k on 10/10/22.
//

#include <SDL2/SDL_ttf.h>
#include <unistd.h>
#include "Main.h"
#include "recalbox-utils/Log.h"
#include "recalbox-utils/HighResolutionTimer.h"

int main(int argc, char* argv[])
{
  Main application;
  application.Run(argc, argv);
  return 0;
}

Main::Main()
  : mConfiguration()
  , mEngine(mConfiguration)
{
}

void Main::SDLLoop()
{
  SDL_Event event;
  HighResolutionTimer timer;
  for(;;)
  {
    // SDL event loop
    while (SDL_PollEvent(&event) != 0)
      switch (event.type)
      {
        case SDL_KEYDOWN:
        case SDL_KEYUP: mEngine.ProcessKeyboard(event.key); break;
        case SDL_JOYBUTTONDOWN:
        case SDL_JOYBUTTONUP:
        case SDL_JOYAXISMOTION:
        case SDL_JOYHATMOTION:
        {
          mEngine.ProcessJoystick(event);
          if (mEngine.QuitRequested()) return;
          break;
        }
        case SDL_QUIT: return;
        default: break;
      }

    // Wait frame time less 1ms
    long long microElapsed = timer.GetMicroSeconds();
    long long timeToWait = 19000 - microElapsed;
    if (timeToWait > 0) usleep(timeToWait);
    timer.Reset();

    // Refresh screen - Wait for emulated machine screen refresh for 5ms maximum
    for(int i = 10; --i >= 0; usleep(500))
      if (mEngine.RequestVideoRefresh()) break;
  }
}

void Main::Run(int argc, char *argv[])
{
  Log::open(nullptr);
  LOG(LogInfo) << "[main] Starting RB5000 Emulator";

  // Initialize configuration first
  mConfiguration.LoadConfiguration(argc, argv);
  // Initialize all the engine
  mEngine.Start();

  // Run SDL loop
  SDLLoop();

  // Quit
  mEngine.Pause();
  SDL_PauseAudio(1);
  SDL_Quit();

  LOG(LogInfo) << "[main] Ending RB5000 Emulator";
  Log::close();
}
