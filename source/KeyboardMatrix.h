//
// Created by bkg2k on 07/10/22.
//
#pragma once

#include <SDL2/SDL.h>
#include "../Z80/Z80.h"

class KeyboardMatrix
{
  public:
    explicit KeyboardMatrix(byte* ioPorts)
      : mIOPorts(ioPorts + sFirstIOPort)
    {
      Reset();
    }

    void ProcessSDL2Event(const SDL_KeyboardEvent& event) const;

  private:
    //! First Keyboard io port
    static constexpr int sFirstIOPort = 0x80;
    //! Max key
    static constexpr int sMaxKeys = 0x64;
    //! Keyboard's IO Port acesss
    byte* mIOPorts;

    void Reset();
};
