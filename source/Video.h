//
// Created by bkg2k on 09/10/22.
//
#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "../Z80/Z80.h"
#include "Configuration.h"
#include "recalbox-utils/gl/Renderer.h"

class Video
{
  public:
    /*!
     * @brief Default constructor
     * @param width Required width
     * @param height Required height
     */
    explicit Video(const Configuration& configuration);

    //! Default destructor
    ~Video();

    /*!
     * @brief Initialize video
     */
    void Initialize();

    /*!
     * @brief At least one frame has been displayed?
     */
    [[nodiscard]] bool HasDisplay() const { return mFrameCounter != 0; }

    /*!
     * @brief A new frame is ready to be displayed
     */
    void FrameReady() { mFrameCounter++; }

    /*!
     * @brief Refresh real window content w/ emulated machine screen
     */
    bool RefreshWindow();

    /*!
     * @brief Draw a char in 40 collumn mode
     * @param charBuffer Char pixel buffer
     * @param x X coordinate
     * @param y Y coordinate
     */
    void DrawChar40(const unsigned char* charBuffer, int x, int y);

    /*!
     * @brief Draw a char in 40 collumn mode
     * @param charBuffer Char pixel buffer
     * @param x X coordinate
     * @param y Y coordinate
     */
    void DrawChar80(const unsigned char*, int, int) {}

    /*!
     * @brief Popup a new message
     * @param message Message
     */
    void Popup(const String& message);

    //! Pixel access
    [[nodiscard]] static const unsigned short* Pixels() { return sPixels; }
    //! Pixel array size
    static constexpr int PixelSize() { return sizeof(sPixels); }

  private:
    static constexpr int sWidth40 = 336;
    static constexpr int sWidth80 = 492;
    static constexpr int sHeight = 270;

    //! Max color
    static constexpr int sColorCount = 16;

    //! RGBA structure
    union Pixel
    {
      struct
      {
        byte b:5; // Blue
        byte g:6; // Green
        byte r:5; // Red
      } RGB __attribute((packed));
      unsigned short V;
    };

    //! GL Renderer
    Renderer mRenderer;

    //! Configuration reference
    const Configuration& mConfiguration;

    //! Font
    TTF_Font* mFont;

    //! Emulated screen texture
    GLuint mPixelsTexture;
    //! Popup text texture
    GLuint mPopupTexture;
    //! Popup text time reference
    unsigned int mPopupReference;
    //! Popup message
    String mPopupMessage;
    //! Popup width
    int mPopupWidth;
    //! Popuop height
    int mPopupHeight;

    //! Pixel array
    static unsigned short sPixels[sHeight * sWidth80];

    //! Real window width
    int mWindowWidth;
    //! Real window height
    int mWindowHeight;

    //! Frame counter
    int mFrameCounter;
    //! Last refreshed frame
    int mLastRefreshedFrame;

    //! Real ratio
    static constexpr double sRealRatio = (double)sWidth40 / (double)sHeight;
    //! Window ratio
    double mWindowRatio;

    //! Palette
    Pixel mPalette[sColorCount];

    /*!
     * @brief Build a color w/ gamma correction
     * @param n Color index
     * @param r Red intensity (0-15)
     * @param g Green intensity (0-15)
     * @param b Blue intensity (0-15)
     */
    static Pixel MakeColor(int r, int g, int b);

    /*!
     * @brief Initialize the whole palette
     */
    void MakePalette();

    /*!
     * @brief Initialize screen/window
     * @param width Requested width
     * @param height Requested height
     * @param windowed Windowed mode (false = fullscreen)
     */
    void InitializeWindow(int width, int height, bool windowed);

    /*!
     * @brief Process popup timer & display
     */
    void DisplayPopup(const Rectangle& targetArea);

    /*!
     * @brief When no resolution is passed to the emulator,
     * try to figure out what's best available resolution to pick up
     * @param width Returned width
     * @param height Returned height
     * @return True if a resolution has been found, false otherwise
     */
    static bool GetBestResolution(int& width, int& height);
};
