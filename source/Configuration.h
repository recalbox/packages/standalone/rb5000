//
// Created by bkg2k on 09/10/22.
//
#pragma once

#include "recalbox-utils/String.h"

class Configuration
{
  public:
    //! Joystick count
    static constexpr int sJoystickCount = 2;
    //! Buttons per player
    static constexpr int sButtonsPerPlayer = 2;

    //! Default constructor
    Configuration()
      : mTapeFileName()
      , mCPUSpeedIndex(sCPUSpeedDefaultIndex)
      , mJoystickIndex { 0, 1 }
      , mJoystickButtons { { 0, 1 }, { 0, 1 } }
      , mJoystickDirectionUp { -1, -1 }
      , mJoystickDirectionDown { -1, -1 }
      , mJoystickDirectionLeft { -1, -1 }
      , mJoystickDirectionRight { -1, -1 }
      , mJoystickHotkey(-1)
      , mJoystickStart(-1)
      , mWindowWidth(-1)
      , mWindowHeight(-1)
      , mSmooth(false)
      , mIntegerScale(false)
      , mTapeAutoRun(false)
      , mTapeWritable(false)
      , mFullscreen(false)
    {
    }

    /*!
     * @brief Load configuration from file & command line
     * @param argc Command line's argument count
     * @param argv Command line's arguments
     */
    void LoadConfiguration(int argc, char* argv[]);

    //! Get Printer file name
    [[nodiscard]] const String& PrinterFileName() const { return mPrinterFileName; }
    //! Get Tape file name
    [[nodiscard]] const String& TapeFileName() const { return mTapeFileName; }
    //! Get Tape autorun
    [[nodiscard]] bool TapeAutoRun() const { return mTapeAutoRun; }
    //! Get Tape writable?
    [[nodiscard]] bool TapeWritable() const { return mTapeWritable; }
    //! Get Joystick 0 index
    [[nodiscard]] int JoystickSDLIndex(int player) const { return mJoystickIndex[player % sJoystickCount]; }
    //! Get Joystick 0 button 0
    [[nodiscard]] int JoystickButtons(int player, int button) const { return mJoystickButtons[player % sJoystickCount][button % sButtonsPerPlayer]; }
    //! Get Joystick up
    [[nodiscard]] int JoystickUp(int player) const { return mJoystickDirectionUp[player % sJoystickCount]; }
    //! Get Joystick down
    [[nodiscard]] int JoystickDown(int player) const { return mJoystickDirectionDown[player % sJoystickCount]; }
    //! Get Joystick left
    [[nodiscard]] int JoystickLeft(int player) const { return mJoystickDirectionLeft[player % sJoystickCount]; }
    //! Get Joystick right
    [[nodiscard]] int JoystickRight(int player) const { return mJoystickDirectionRight[player % sJoystickCount]; }
    //! Get Joystick 0 hotkey
    [[nodiscard]] int JoystickHotkey() const { return mJoystickHotkey; }
    //! Get Joystick 0 start
    [[nodiscard]] int JoystickStart() const { return mJoystickStart; }
    //! Get CPU Speed index
    [[nodiscard]] int CPUSpeedIndex() const { return mCPUSpeedIndex; }
    //! Get CPU Speed in KHz
    [[nodiscard]] int CPUSpeedKHz() const { return sCPUSpeedFromIndex[mCPUSpeedIndex]; }
    //! Window width
    [[nodiscard]] int WindowWidth() const { return mWindowWidth; }
    //! Window height
    [[nodiscard]] int WindowHeight() const { return mWindowHeight; }
    //! Smooth
    [[nodiscard]] bool Smooth() const { return mSmooth; }
    //! Integer scaling
    [[nodiscard]] bool IntegerScale() const { return mIntegerScale; }
    //! Fullscreen
    [[nodiscard]] bool Fullscreen() const { return mFullscreen; }

    //! Speed up CPU
    void SpeedUp() { if (mCPUSpeedIndex < sCPUSpeedCount - 1) ++mCPUSpeedIndex; }
    //! Slow down CPU
    void SlowDown() { if (--mCPUSpeedIndex < 0) mCPUSpeedIndex = 0; }
    //! Set CPU speed to default
    void DefaultSpeed() { mCPUSpeedIndex = sCPUSpeedDefaultIndex; }

  private:
    //! Printer file
    String mPrinterFileName;
    //! K7 file
    String mTapeFileName;
    //! CPU Speed index
    int mCPUSpeedIndex;
    //! Joystick's SDL2 index
    int mJoystickIndex[sJoystickCount];
    //! Joystick buttons (button ids)
    int mJoystickButtons[sJoystickCount][sButtonsPerPlayer];
    //! Joystick direction up buttons (button ids)
    int mJoystickDirectionUp[sJoystickCount];
    //! Joystick direction down buttons (button ids)
    int mJoystickDirectionDown[sJoystickCount];
    //! Joystick direction left buttons (button ids)
    int mJoystickDirectionLeft[sJoystickCount];
    //! Joystick direction right buttons (button ids)
    int mJoystickDirectionRight[sJoystickCount];
    //! Joystick 0 Hotkey
    int mJoystickHotkey;
    //! Joystick 0 Start
    int mJoystickStart;
    //! Window width
    int mWindowWidth;
    //! Window height
    int mWindowHeight;
    //! Smooth
    bool mSmooth;
    //! Integer Scale
    bool mIntegerScale;
    //! Autorun tape
    bool mTapeAutoRun;
    //! Tape writable?
    bool mTapeWritable;
    //! Fullscreen
    bool mFullscreen;

    //! CPU Speeds
    static constexpr int sCPUSpeedCount = 10;
    //! CPU Speeds
    static constexpr int sCPUSpeedDefaultIndex = 5;
    //! CPU Speed array
    static int sCPUSpeedFromIndex[sCPUSpeedCount];

    /*!
     * @brief Read configuration from file
     * @param fileName fileName
     */
    void ReadFromFile(const String& fileName);

    /*!
     * @brief Get configuration filename from command line
     * or return default one
     * @param argc Command line's argument count
     * @param argv Command line's arguments
     * @return Config filename
     */
    static String GetFileFromCommandLine(int argc, char* argv[]);

    /*!
     * @brief Get configuration from command line
     * @param argc Command line's argument count
     * @param argv Command line's arguments
     */
    void ReadFromCommandLine(int argc, char* argv[]);
};
